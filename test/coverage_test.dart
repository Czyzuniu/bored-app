/// *** GENERATED FILE - ANY CHANGES WOULD BE OBSOLETE ON NEXT GENERATION *** ///

/// Helper to test coverage for all project files
// ignore_for_file: unused_import
import 'package:bored/main.dart';
import 'package:bored/device/repositories/get_current_location_data_source.dart';
import 'package:bored/device/repositories/get_network_info_repository_impl.dart';
import 'package:bored/device/repositories/get_current_location_repository_impl.dart';
import 'package:bored/dependency_injection/dependency_container.dart';
import 'package:bored/dependency_injection/dependency_container_injection_module.dart';
import 'package:bored/data/weather/datasources/get_weather_remote_data_source.dart';
import 'package:bored/data/weather/repositories/get_weather_repository_impl.dart';
import 'package:bored/data/weather/models/weather_dto.dart';
import 'package:bored/data/weather/models/get_weather_dto.dart';
import 'package:bored/data/find_activity/datasources/find_activity_remote_data_source.dart';
import 'package:bored/data/find_activity/mapper/activity_dto_mapper.dart';
import 'package:bored/data/find_activity/mapper/search_activity_dto_mapper.dart';
import 'package:bored/data/find_activity/repositories/find_activity_repository_impl.dart';
import 'package:bored/data/find_activity/models/activity_dto.dart';
import 'package:bored/data/find_activity/models/find_activity_dto.dart';
import 'package:bored/domain/core/repositories/get_network_info_repository.dart';
import 'package:bored/domain/core/repositories/get_current_location_repository.dart';
import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/utils/input_converter.dart';
import 'package:bored/domain/weather/repositories/get_weather_repository.dart';
import 'package:bored/domain/weather/usecase/get_weather_by_localization.dart';
import 'package:bored/domain/weather/entities/weather.dart';
import 'package:bored/domain/find_activity/repositories/find_activity_repository.dart';
import 'package:bored/domain/find_activity/usecases/find_random_activity.dart';
import 'package:bored/domain/find_activity/usecases/find_activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:bored/presentation/app_drawer.dart';
import 'package:bored/presentation/weather/cubit/weather_cubit.dart';
import 'package:bored/presentation/weather/pages/weather_page.dart';
import 'package:bored/presentation/weather/pages/weather_page_view.dart';
import 'package:bored/presentation/find_activity/pages/find_activity_page.dart';
import 'package:bored/presentation/find_activity/pages/find_activity_page_view.dart';
import 'package:bored/presentation/find_activity/widgets/find_activity_type_dropdown.dart';
import 'package:bored/presentation/find_activity/widgets/find_activity_participants_input.dart';
import 'package:bored/presentation/find_activity/widgets/activity_type_form_controls.dart';
import 'package:bored/presentation/find_activity/widgets/message_display.dart';
import 'package:bored/presentation/find_activity/widgets/find_activity_submit_button.dart';
import 'package:bored/presentation/find_activity/widgets/find_random_activity_submit_button.dart';
import 'package:bored/presentation/core/loading_widget.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart';

void main() {}
