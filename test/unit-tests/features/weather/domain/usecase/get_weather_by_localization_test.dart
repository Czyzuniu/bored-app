import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/core/repositories/get_current_location_repository.dart';
import 'package:bored/domain/weather/entities/weather.dart';
import 'package:bored/domain/weather/repositories/get_weather_repository.dart';
import 'package:bored/domain/weather/usecase/get_weather_by_localization.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockGetWeatherRepository extends Mock implements GetWeatherRepository {}

class MockGetCurrentLocalizationRepository extends Mock
    implements GetCurrentLocalizationRepository {}

void main() {
  late GetWeatherRepository repository;
  late GetCurrentLocalizationRepository currentLocalizationRepository;
  late GetWeatherByLocalization useCase;

  const coords = CoordinativeLocation(latitude: 50.0, longitude: 50.0);

  setUp(() {
    repository = MockGetWeatherRepository();
    currentLocalizationRepository = MockGetCurrentLocalizationRepository();
    useCase =
        GetWeatherByLocalization(repository, currentLocalizationRepository);
  });

  const tWeather = Weather(celcius: 25);

  test('should call the repository and return the result', () async {
    when(() => currentLocalizationRepository.getLocation())
        .thenAnswer((invocation) async => right(coords));
    when(() => repository.getWeatherByCoordinate(coords))
        .thenAnswer((invocation) async => right(tWeather));

    final result = await useCase.execute();

    expect(result, right<dynamic, Weather>(tWeather));
    verify(() => repository.getWeatherByCoordinate(coords));
    verifyNoMoreInteractions(repository);
  });
}
