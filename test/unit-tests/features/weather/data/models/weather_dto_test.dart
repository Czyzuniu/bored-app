import 'package:bored/data/weather/models/get_weather_dto.dart';
import 'package:test/test.dart';
import '../../../../fixtures/fixture_reader.dart';
import 'dart:convert';


void main() {
  test('should convert to weather dto from json', () {
    final result = GetWeatherDto.fromJson(jsonDecode(fixture('weather.json')) as Map<String, dynamic>);

    // assert
    expect(result.weatherDto.temperature, 13.3);
  });
}