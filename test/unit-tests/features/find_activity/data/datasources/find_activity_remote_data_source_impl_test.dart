import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:bored/data/find_activity/datasources/find_activity_remote_data_source.dart';
import 'package:bored/data/find_activity/models/find_activity_dto.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';

import '../../../../fixtures/fixture_reader.dart';


class MockHttpClient extends Mock implements http.Client {}

class FakeUri extends Fake implements Uri {}

void main() {
  late FindActivityRemoteDataSource remoteDataSource;
  late MockHttpClient mockHttpClient;

  const tActivityType = ActivityType.relaxation;
  const tParticipants = 5;
  final tFindActivityDto = FindActivityDto(type: tActivityType.name, participants: tParticipants.toString());

  setUpAll(() {
    registerFallbackValue(Uri());
    mockHttpClient = MockHttpClient();
    remoteDataSource = FindActivityRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(() => mockHttpClient.get(any(that: isNotNull),
            headers: any(named: 'headers')))
        .thenAnswer(
            (invocation) async => http.Response(fixture('activity.json'), 200));
  }

  void setUpMockHttpClientSuccess200WithNoActivityFoundResponse() {
    when(() => mockHttpClient.get(any(that: isNotNull),
            headers: any(named: 'headers')))
        .thenAnswer((invocation) async =>
            http.Response(fixture('error_activity.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(() => mockHttpClient.get(any(that: isNotNull),
            headers: any(named: 'headers')))
        .thenAnswer(
            (invocation) async => http.Response('Something went wrong', 404));
  }

  group('get specific activity', () {
    test(
      'should fetch and return the ActivityDto from the API by type',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await remoteDataSource.findActivity(tFindActivityDto);

        // assert
        verify(() => mockHttpClient.get(
            Uri.https('www.boredapi.com', '/api/activity', <String, dynamic>{
              'type': tActivityType.name,
              'participants': tParticipants.toString()
            }),
            headers: {'Content-Type': 'application/json'}));
        expect(result.type, ActivityType.busywork);
        expect(result.activity, 'Clean out your car');
        expect(result.participants, 1);
      },
    );

    test(
        'should throw NoRecordException when unable to parse the response JSON',
        () async {
      // arrange
      setUpMockHttpClientSuccess200WithNoActivityFoundResponse();

      // assert
      expect(() async => await remoteDataSource.findActivity(tFindActivityDto),
          throwsA(isA<NoRecordException>()));
    });

    test(
      'should throw a server exception if 200 is not returned',
      () async {
        // arrange
        setUpMockHttpClientFailure404();
        // act => assert
        expect(
            () async => await remoteDataSource.findActivity(tFindActivityDto),
            throwsA(isA<ServerException>()));
      },
    );
  });

  group('get activity by random', () {
    test(
      'should fetch and return the ActivityDto from the API by random',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await remoteDataSource.findAnyRandomActivity();

        // assert
        verify(() => mockHttpClient.get(
            Uri.https('www.boredapi.com', '/api/activity'),
            headers: {'Content-Type': 'application/json'}));
        expect(result.type, ActivityType.busywork);
        expect(result.activity, 'Clean out your car');
        expect(result.participants, 1);
      },
    );

    test(
      'should throw a server exception if 200 is not returned',
      () async {
        // arrange
        setUpMockHttpClientFailure404();
        // act => assert
        expect(() async => await remoteDataSource.findAnyRandomActivity(),
            throwsA(isA<ServerException>()));
      },
    );
  });
}
