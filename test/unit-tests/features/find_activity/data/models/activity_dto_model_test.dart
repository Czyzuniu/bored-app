import 'dart:convert';

import 'package:bored/data/find_activity/models/activity_dto.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {

  test(
    'should read ActivityDto from JSON',
    () async {
      // act
      final result = ActivityDto.fromJson(jsonDecode(fixture('activity.json')) as Map<String, dynamic>);

      // assert
      expect(result.type, ActivityType.busywork);
      expect(result.participants, 1);
      expect(result.activity, 'Clean out your car');
    },
  );

  test(
    'should convert ActivityDto to JSON',
    () async {
      // arrange
      const expectedMap = {
        'type': 'relaxation',
        'participants': 1,
        'activity': 'test'
      };

      // act
      final result = const ActivityDto(
              activity: 'test', type: ActivityType.relaxation, participants: 1)
          .toJson();

      // assert
      expect(result, expectedMap);
    },
  );
}
