import 'package:bored/data/find_activity/mapper/activity_dto_mapper.dart';
import 'package:bored/data/find_activity/models/activity_dto.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    'should map to Activity from ActivityDto',
    () async {
      // arrange

      // act
      Activity result = ActivityDtoMapperImpl().toActivity(const ActivityDto(activity: 'this is a test', type: ActivityType.relaxation, participants: 5));

      // assert
      expect(result.activity, 'this is a test');
      expect(result.participants, 5);
      expect(result.activityType, ActivityType.relaxation);

    },
  );
}
