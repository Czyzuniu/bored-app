import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/data/find_activity/datasources/find_activity_remote_data_source.dart';
import 'package:bored/data/find_activity/mapper/activity_dto_mapper.dart';
import 'package:bored/data/find_activity/mapper/search_activity_dto_mapper.dart';
import 'package:bored/data/find_activity/models/activity_dto.dart';
import 'package:bored/data/find_activity/models/find_activity_dto.dart';
import 'package:bored/data/find_activity/repositories/find_activity_repository_impl.dart';
import 'package:bored/domain/core/repositories/get_network_info_repository.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockFindActivityRemoteDataSource extends Mock implements FindActivityRemoteDataSource {}
class MockNetworkInfo extends Mock implements GetNetworkInfoRepository {}

void main() {
  late FindActivityRepositoryImpl repository;
  late FindActivityRemoteDataSource mockFindActivityRemoteDataSource;
  late GetNetworkInfoRepository mockNetworkInfo;

  setUp(() {
    mockFindActivityRemoteDataSource = MockFindActivityRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = FindActivityRepositoryImpl(
        remoteDataSource: mockFindActivityRemoteDataSource,
        networkInfo: mockNetworkInfo,
        activityDtoMapper: ActivityDtoMapperImpl(),
        searchActivityMapper: SearchActivityMapperImpl()
    );
  });

  group('find activity by type', () {
    const tActivityType = ActivityType.relaxation;
    const tParticipants = 5;
    const tActivityText = 'test';
    const tActivityDto = ActivityDto(
        activity: tActivityText,
        type: tActivityType,
        participants: tParticipants);
    const tActivity = Activity(
        participants: tParticipants,
        activityType: tActivityType,
        activity: tActivityText);


    const tSearchActivity = SearchActivity(activityType: tActivityType, participants: tParticipants);
    final tFindActivityDto = FindActivityDto(type: tActivityType.name, participants: tParticipants.toString());

    group('device is online', () {
      setUp(() {
        when(() => mockNetworkInfo.isConnected)
            .thenAnswer((_) async => Future.value(true));
      });

      group('filtered activity', () {
        test(
          'should return remote data by type when online',
              () async {
            // arrange
            when(() => mockFindActivityRemoteDataSource
                .findActivity(tFindActivityDto))
                .thenAnswer((realInvocation) async => tActivityDto);
            // act
            final result = await repository.findActivity(tSearchActivity);
            // assert
            verify(() => mockFindActivityRemoteDataSource
                .findActivity(tFindActivityDto));
            expect(result, equals(const Right<dynamic, Activity>(tActivity)));
          },
        );

        test(
          'should return a Failure.server if remote data source throws a server exception',
              () async {
            // arrange
            final expectedException = ServerException('Server Failure');

            when(() => mockFindActivityRemoteDataSource
                .findActivity(tFindActivityDto))
                .thenThrow(expectedException);
            // act
            final result = await repository.findActivity(tSearchActivity);

            // assert
            expect(result, equals(Left<dynamic, Failure>(Failure.server(expectedException.message))));
          },
        );

        test(
          'should return a NoActivityFoundFailure Failure when a NoRecordException is thrown',
              () async {
            // arrange
            final expectedException = NoRecordException('No record');

            when(() => mockFindActivityRemoteDataSource
                .findActivity(tFindActivityDto))
                .thenThrow(expectedException);
            // act
            final result = await repository.findActivity(tSearchActivity);

            // assert
            expect(result, equals(Left<dynamic, Failure>(Failure.noActivityFound(expectedException.message))));
          },
        );
      });

      group('random activity', () {
        test(
          'should return activity by random',
              () async {
            // arrange
            when(() => mockFindActivityRemoteDataSource
                .findAnyRandomActivity())
                .thenAnswer((realInvocation) async => tActivityDto);
            // act
            final result = await repository.findRandomActivity();
            // assert
            verify(() => mockFindActivityRemoteDataSource
                .findAnyRandomActivity());
            expect(result, equals(const Right<dynamic, Activity>(tActivity)));
          },
        );

        test(
          'should return a Failure.server if remote data source throws a server exception',
              () async {
            // arrange
            final expectedException = ServerException('Server Failure');

            when(() => mockFindActivityRemoteDataSource
                .findAnyRandomActivity())
                .thenThrow(expectedException);
            // act
            final result = await repository.findRandomActivity();

            // assert
            expect(result, equals(Left<dynamic, Failure>(Failure.server(expectedException.message))));
          },
        );

        test(
          'should return a NoActivityFoundFailure Failure when a NoRecordException is thrown',
              () async {
            // arrange
            final expectedException = NoRecordException('No record');

            when(() => mockFindActivityRemoteDataSource
                .findAnyRandomActivity())
                .thenThrow(expectedException);
            // act
            final result = await repository.findRandomActivity();

            // assert
            expect(result, equals(Left<dynamic, Failure>(Failure.noActivityFound(expectedException.message))));
          },
        );
      });
    });

    group('device is offline', () {
      setUp(() {
        when(() => mockNetworkInfo.isConnected)
            .thenAnswer((_) async => Future.value(false));
      });

      test(
        'should return NoInternetFailure if not connected to the internet',
        () async {
          // act
          final result = await repository.findActivity(tSearchActivity);
          // assert
          expect(result, equals(const Left<dynamic, Failure>(Failure.noInternet('There is no internet.'))));
        },
      );
    });
  });
}
