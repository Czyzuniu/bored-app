import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:bored/domain/find_activity/repositories/find_activity_repository.dart';
import 'package:bored/domain/find_activity/usecases/find_activity.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';


class MockFindActivityRepository extends Mock implements FindActivityRepository {}

void main() {
  late FindActivity useCase;
  late FindActivityRepository mockFindActivityRepository;

  setUp(() {
    mockFindActivityRepository = MockFindActivityRepository();
    useCase = FindActivity(mockFindActivityRepository);
  });

  const ActivityType tActivityType = ActivityType.recreational;
  const SearchActivity tSearchActivity = SearchActivity(activityType: tActivityType, participants: 5);
  const Activity tActivity = Activity(activity: 'test', participants: 5, activityType: tActivityType);

  test(
    'should get activity from repository ',
    () async {
      // arrange
      when(() => mockFindActivityRepository.findActivity(tSearchActivity)).thenAnswer((_) async => const Right(tActivity));

      // act
      final result = await useCase.execute(tSearchActivity);

      // assert

      expect(result, const Right<dynamic, Activity>(tActivity));
      verify(() => mockFindActivityRepository.findActivity(tSearchActivity));
      verifyNoMoreInteractions(mockFindActivityRepository);
    },
  );
}