import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/domain/find_activity/repositories/find_activity_repository.dart';
import 'package:bored/domain/find_activity/usecases/find_random_activity.dart';
import 'package:dartz/dartz.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

class MockFindActivityRepository extends Mock implements FindActivityRepository {}

void main() {
  late FindRandomActivity useCase;
  late FindActivityRepository mockFindActivityRepository;

  const Activity tActivity = Activity(activity: 'test', participants: 5, activityType: ActivityType.busywork);

  setUp(() {
    mockFindActivityRepository = MockFindActivityRepository();
    useCase = FindRandomActivity(mockFindActivityRepository);
  });

  test(
    'should get activity from repository ',
        () async {
      // arrange
      when(() => mockFindActivityRepository.findRandomActivity()).thenAnswer((_) async => const Right(tActivity));

      // act
      final result = await useCase.execute();

      // assert
      expect(result, const Right<dynamic, Activity>(tActivity));
      verify(() => mockFindActivityRepository.findRandomActivity());
      verifyNoMoreInteractions(mockFindActivityRepository);
    },
  );
}