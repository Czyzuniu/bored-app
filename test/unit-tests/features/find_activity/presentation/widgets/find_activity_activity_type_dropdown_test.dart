import 'package:bloc_test/bloc_test.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:bored/presentation/find_activity/widgets/find_activity_type_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockFindActivityInputBloc
    extends MockBloc<FindActivityInputEvent, FindActivityInputState>
    implements FindActivityInputBloc {}

void main() {
  late FindActivityInputBloc findActivityInputBloc;
  setUp(() {
    findActivityInputBloc = MockFindActivityInputBloc();
  });

  group('activity type dropdown widget tests', () {
    testWidgets('should emit a FindActivityActivityTypeChanged when changed',
        (WidgetTester tester) async {
      when(() => findActivityInputBloc.state).thenReturn(
        const FindActivityInputState(
            selectedActivityType: ActivityType.recreational,
            selectedParticipants: '1'),
      );

      // arrange
      const inputId = Key('ActivityTypeDropdown');
      const actualInput = ActivityType.busywork;

      // arrange
      await tester.pumpWidget(BlocProvider.value(
        value: findActivityInputBloc,
        child: BlocBuilder<FindActivityInputBloc, FindActivityInputState>(
          builder: (context, state) {
            return const MaterialApp(
                home: Material(child: FindActivityTypeDropdown()));
          },
        ),
      ));

      // act
      final dropdown = find.byKey(inputId);

      await tester.tap(dropdown);
      await tester.pumpAndSettle();

      final dropdownItem = find.text(actualInput.name).last;

      await tester.tap(dropdownItem);

      // assert
      verify(() => findActivityInputBloc
          .add(const FindActivityActivityTypeChanged(actualInput)));
    });
  });
}
