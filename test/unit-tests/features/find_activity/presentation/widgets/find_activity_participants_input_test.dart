import 'package:bloc_test/bloc_test.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:bored/presentation/find_activity/widgets/find_activity_participants_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockFindActivityInputBloc
    extends MockBloc<FindActivityInputEvent, FindActivityInputState>
    implements FindActivityInputBloc {}

void main() {
  late FindActivityInputBloc findActivityInputBloc;
  setUp(() {
    findActivityInputBloc = MockFindActivityInputBloc();
  });

  group('participants input widget tests', () {
    testWidgets(
        'should emit a FindActivityParticipantsInputChanged event when changed',
        (WidgetTester tester) async {
      when(() => findActivityInputBloc.state).thenReturn(
        const FindActivityInputState(
            selectedActivityType: ActivityType.recreational,
            selectedParticipants: '1'), // the desired state
      );

      // arrange
      const inputId = Key('ParticipantsInput');
      const actualInput = '5';

      // arrange
      await tester.pumpWidget(BlocProvider.value(
        value: findActivityInputBloc,
        child: BlocBuilder<FindActivityInputBloc, FindActivityInputState>(
          builder: (context, state) {
            return const MaterialApp(
                home: Material(child: FindActivityParticipantsInput(key: Key('FindActivityParticipantsInput'),)));
          },
        ),
      ));

      // act
      await tester.enterText(find.byKey(inputId), actualInput);

      // assert
      verify(() => findActivityInputBloc
          .add(const FindActivityParticipantsInputChanged(actualInput)));
    });
  });
}
