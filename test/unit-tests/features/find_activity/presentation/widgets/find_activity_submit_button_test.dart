import 'package:bloc_test/bloc_test.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart';
import 'package:bored/presentation/find_activity/widgets/find_activity_submit_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockFindActivityInputBloc
    extends MockBloc<FindActivityInputEvent, FindActivityInputState>
    implements FindActivityInputBloc {}

class MockDisplayActivityBloc
    extends MockBloc<FindActivitySubmitEvent, FindActivitySubmitState>
    implements FindActivitySubmitBloc {}

void main() {
  late FindActivityInputBloc findActivityInputBloc;
  late FindActivitySubmitBloc findActivitySubmitBloc;
  setUp(() {
    findActivityInputBloc = MockFindActivityInputBloc();
    findActivitySubmitBloc = MockDisplayActivityBloc();
  });

  group('find activity submit button tests', () {
    testWidgets('should emit a FindActivitySubmitPressed when tapped',
        (WidgetTester tester) async {
      const selectedType = ActivityType.busywork;
      const selectedParticipants = '5';

      when(() => findActivityInputBloc.state).thenReturn(
        const FindActivityInputState(
            selectedActivityType: selectedType,
            selectedParticipants: selectedParticipants), // the desired state
      );

      when(() => findActivitySubmitBloc.state)
          .thenReturn(const FindActivitySubmitState.initial());

      // arrange
      await tester.pumpWidget(MultiBlocProvider(
          providers: [
            BlocProvider<FindActivityInputBloc>(
              create: (BuildContext context) => findActivityInputBloc,
            ),
            BlocProvider<FindActivitySubmitBloc>(
              create: (BuildContext context) => findActivitySubmitBloc,
            )
          ],
          child: BlocBuilder<FindActivitySubmitBloc, FindActivitySubmitState>(
            builder: (context, state) {
              return const MaterialApp(
                  home: Material(child: FindActivitySubmitButton()));
            },
          )));

      // act
      await tester.tap(find.byKey(const Key('SubmitFindActivityButton')));

      // assert
      verify(() => findActivitySubmitBloc.add(const FindActivitySubmitEvent.findActivitySubmitPressed(selectedType, selectedParticipants)));
    });

    testWidgets('should have no onPressed when [Loading] to make it disabled',
        (WidgetTester tester) async {
      // arrange
      const buttonKey = Key('SubmitFindActivityButton');

      when(() => findActivitySubmitBloc.state).thenReturn(
        const FindActivitySubmitState.loading(),
      );

      await tester.pumpWidget(MultiBlocProvider(
          providers: [
            BlocProvider<FindActivityInputBloc>(
              create: (BuildContext context) => findActivityInputBloc,
            ),
            BlocProvider<FindActivitySubmitBloc>(
              create: (BuildContext context) => findActivitySubmitBloc,
            )
          ],
          child: BlocBuilder<FindActivitySubmitBloc, FindActivitySubmitState>(
            builder: (context, state) {
              return const MaterialApp(
                  home: Material(child: FindActivitySubmitButton()));
            },
          )));

      // act
      final button = tester.widget<ElevatedButton>(find.byKey(buttonKey));

      // assert
      expect(button.onPressed, isNull);
    });
  });
}
