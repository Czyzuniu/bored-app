import 'package:bloc_test/bloc_test.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {
  late FindActivityInputBloc bloc;

  setUp(() {
    bloc = FindActivityInputBloc();
  });

  const tActivityType = ActivityType.recreational;
  const tParticipants = '5';

  group('find activity bloc input state', () {

    blocTest<FindActivityInputBloc, FindActivityInputState>(
        'should set activity type  when a FindActivityActivityTypeChanged is emitted',
        build: () {
          return bloc;
        },
        act: (bloc) =>
            bloc.add(const FindActivityActivityTypeChanged(tActivityType)),
        expect: () =>
        [
          const FindActivityInputState(selectedActivityType: tActivityType,
              selectedParticipants: '1')
        ]
    );

    blocTest<FindActivityInputBloc, FindActivityInputState>(
        'should set participants when a FindActivityParticipantsInputChanged is emitted',
        build: () {
          return bloc;
        },
        act: (bloc) =>
            bloc.add(const FindActivityParticipantsInputChanged(tParticipants)),
        expect: () =>
        [
          const FindActivityInputState(
              selectedActivityType: ActivityType.recreational,
              selectedParticipants: tParticipants)
        ]
    );
  });
}
