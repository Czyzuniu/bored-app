import 'package:bloc_test/bloc_test.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:bored/domain/find_activity/usecases/find_activity.dart';
import 'package:bored/domain/find_activity/usecases/find_random_activity.dart';
import 'package:bored/domain/utils/input_converter.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockFindActivity extends Mock implements FindActivity {}

class MockFindRandomActivity extends Mock implements FindRandomActivity {}

class MockInputConverter extends Mock implements InputConverter {}

void main() {
  late FindActivitySubmitBloc bloc;
  late MockFindActivity findActivity;
  late MockFindRandomActivity findRandomActivity;
  late MockInputConverter inputConverter;

  setUp(() {
    findActivity = MockFindActivity();
    inputConverter = MockInputConverter();
    findRandomActivity = MockFindRandomActivity();

    bloc = FindActivitySubmitBloc(
        findActivity, inputConverter, findRandomActivity);
  });

  group('find activity submit bloc', () {
    const invalidInputFailureMessage = 'INVALID_INPUT';
    const tActivityType = ActivityType.cooking;
    const tSearchActivity =
        SearchActivity(activityType: tActivityType, participants: 5);
    const tActivity = Activity(
        activity: 'test', activityType: tActivityType, participants: 5);

    group('filtered activity', () {
      blocTest<FindActivitySubmitBloc, FindActivitySubmitState>(
        'emits [Loading -> Error] when participants input is not valid',
        build: () {
          when(() => inputConverter.stringToPositiveInteger('error'))
              .thenReturn(
                  const Left(Failure.invalidInput(invalidInputFailureMessage)));
          return bloc;
        },
        act: (bloc) => bloc.add(
            const FindActivitySubmitEvent.findActivitySubmitPressed(
                tActivityType, 'error')),
        expect: () => [
          const FindActivitySubmitState.loading(),
          const FindActivitySubmitState.error(invalidInputFailureMessage)
        ],
      );

      blocTest<FindActivitySubmitBloc, FindActivitySubmitState>(
          'should emit [Loading, Loaded(Activity)] when a call to use case is successful',
          build: () {
            when(() => inputConverter.stringToPositiveInteger('5'))
                .thenReturn(const Right(5));
            when(() => findActivity.execute(tSearchActivity))
                .thenAnswer((invocation) async => const Right(tActivity));
            return bloc;
          },
          act: (bloc) => bloc.add(
              const FindActivitySubmitEvent.findActivitySubmitPressed(
                  tActivityType, '5')),
          expect: () => [
                const FindActivitySubmitState.loading(),
                const FindActivitySubmitState.loaded(tActivity)
              ],
          verify: (_) {
            verify(() => findActivity.execute(tSearchActivity));
          });

      blocTest<FindActivitySubmitBloc, FindActivitySubmitState>(
          'should emit [Loading, Error] when a call to use case is unsuccessful',
          build: () {
            when(() => inputConverter.stringToPositiveInteger('5'))
                .thenReturn(const Right(5));
            when(() => findActivity.execute(tSearchActivity)).thenAnswer(
                (invocation) async =>
                    const Left(Failure.server('SERVER_FAILURE')));
            return bloc;
          },
          act: (bloc) => bloc.add(
              const FindActivitySubmitEvent.findActivitySubmitPressed(
                  tActivityType, '5')),
          expect: () => [
                const FindActivitySubmitState.loading(),
                const FindActivitySubmitState.error('SERVER_FAILURE')
              ],
          verify: (_) {
            verify(() => findActivity.execute(tSearchActivity));
          });
    });

    group('random activity', () {
      blocTest<FindActivitySubmitBloc, FindActivitySubmitState>(
          'should emit [Loading, Loaded(Activity)] when a call to use case is successful',
          build: () {
            when(() => findRandomActivity.execute())
                .thenAnswer((invocation) async => const Right(tActivity));
            return bloc;
          },
          act: (bloc) => bloc.add(
              const FindActivitySubmitEvent.findActivitySubmitRandomPressed()),
          expect: () => [
                const FindActivitySubmitState.loading(),
                const FindActivitySubmitState.loaded(tActivity)
              ],
          verify: (_) {
            verify(() => findRandomActivity.execute());
          });

      blocTest<FindActivitySubmitBloc, FindActivitySubmitState>(
          'should emit [Loading, Error] when a call to use case is unsuccessful',
          build: () {
            when(() => findRandomActivity.execute()).thenAnswer(
                (invocation) async =>
                    const Left(Failure.server('SERVER_FAILURE')));
            return bloc;
          },
          act: (bloc) => bloc.add(const FindActivitySubmitRandomPressed()),
          expect: () => [
                const FindActivitySubmitState.loading(),
                const FindActivitySubmitState.error('SERVER_FAILURE')
              ],
          verify: (_) {
            verify(() => findRandomActivity.execute());
          });
    });
  });
}
