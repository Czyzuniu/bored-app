import 'package:bored/device/repositories/get_network_info_repository_impl.dart';
import 'package:bored/domain/core/repositories/get_network_info_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mocktail/mocktail.dart';

class MockInternetConnectionChecker extends Mock implements InternetConnectionChecker {}

void main() {
  late MockInternetConnectionChecker mockInternetConnectionChecker;
  late GetNetworkInfoRepository networkInfoImpl;

  setUp(() {
    mockInternetConnectionChecker = MockInternetConnectionChecker();
    networkInfoImpl = NetworkInfoImpl(mockInternetConnectionChecker);
  });

  test('should forward the call to InternetConnectionChecker', () {
    // arrange
    final tHasConnectionFuture = Future.value(true);

    when(() => mockInternetConnectionChecker.hasConnection)
        .thenAnswer((_) => tHasConnectionFuture);
    // act
    final result = networkInfoImpl.isConnected;
    // assert
    verify(() => mockInternetConnectionChecker.hasConnection);
    expect(result, tHasConnectionFuture);
  });
}