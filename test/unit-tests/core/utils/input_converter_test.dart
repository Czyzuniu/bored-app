import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/utils/input_converter.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  late InputConverter converter;

  setUp(() {
    converter = InputConverter();
  });

  group('string to positive integer conversion', () {
    test(
      'should return an integer when the string represents an unsigned integer',
      () {
        // Arrange
        const inputString = '5';
        // Act
        final result = converter.stringToPositiveInteger(inputString);
        // Assert
        expect(result, const Right<dynamic, int>(5));
      },
    );

    test(
      'should return a [IntegerMustBeHigherThanZeroFailure] if parsed string is not a positive integer',
      () {
        // Arrange
        const inputString = '-5';
        // Act
        final result = converter.stringToPositiveInteger(inputString);

        // Assert
        expect(result, const Left<Failure, dynamic>(Failure.invalidInput('Unable to parse the number.')));
      },
    );

    test(
      'should return a [ParseStringToIntFailure] if string cannot be parsed to an integer',
      () {
        // Arrange
        const inputString = 'notValidString';
        // Act
        final result = converter.stringToPositiveInteger(inputString);

        // Assert
        expect(result, const Left<Failure, dynamic>(Failure.invalidInput('Unable to parse the number.')));
      },
    );
  });
}
