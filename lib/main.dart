import 'package:bored/dependency_injection/dependency_container.dart';
import 'package:bored/presentation/find_activity/pages/find_activity_page_view.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Do Something!',
      theme: ThemeData(
        primaryColor: Colors.green.shade800,
        colorScheme:
            ColorScheme.fromSwatch().copyWith(secondary: Colors.green.shade600),
        fontFamily: 'Arial'
      ),
      home: const FindActivityPageView(),
    );
  }
}
