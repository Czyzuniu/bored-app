part of 'find_activity_input_bloc.dart';

@freezed
class FindActivityInputState with _$FindActivityInputState {
  const factory FindActivityInputState(
      {required ActivityType selectedActivityType,
      required String selectedParticipants}) = _FindActivityInputState;

  factory FindActivityInputState.initial() => const FindActivityInputState(
      selectedActivityType: ActivityType.recreational,
      selectedParticipants: '1');
}
