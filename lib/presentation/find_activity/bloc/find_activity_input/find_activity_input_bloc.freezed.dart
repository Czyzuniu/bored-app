// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'find_activity_input_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FindActivityInputEventTearOff {
  const _$FindActivityInputEventTearOff();

  FindActivityActivityTypeChanged findActivityActivityTypeChanged(
      ActivityType activityType) {
    return FindActivityActivityTypeChanged(
      activityType,
    );
  }

  FindActivityParticipantsInputChanged findActivityParticipantsInputChanged(
      String participantsInput) {
    return FindActivityParticipantsInputChanged(
      participantsInput,
    );
  }
}

/// @nodoc
const $FindActivityInputEvent = _$FindActivityInputEventTearOff();

/// @nodoc
mixin _$FindActivityInputEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ActivityType activityType)
        findActivityActivityTypeChanged,
    required TResult Function(String participantsInput)
        findActivityParticipantsInputChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ActivityType activityType)?
        findActivityActivityTypeChanged,
    TResult Function(String participantsInput)?
        findActivityParticipantsInputChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ActivityType activityType)?
        findActivityActivityTypeChanged,
    TResult Function(String participantsInput)?
        findActivityParticipantsInputChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FindActivityActivityTypeChanged value)
        findActivityActivityTypeChanged,
    required TResult Function(FindActivityParticipantsInputChanged value)
        findActivityParticipantsInputChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FindActivityActivityTypeChanged value)?
        findActivityActivityTypeChanged,
    TResult Function(FindActivityParticipantsInputChanged value)?
        findActivityParticipantsInputChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FindActivityActivityTypeChanged value)?
        findActivityActivityTypeChanged,
    TResult Function(FindActivityParticipantsInputChanged value)?
        findActivityParticipantsInputChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivityInputEventCopyWith<$Res> {
  factory $FindActivityInputEventCopyWith(FindActivityInputEvent value,
          $Res Function(FindActivityInputEvent) then) =
      _$FindActivityInputEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$FindActivityInputEventCopyWithImpl<$Res>
    implements $FindActivityInputEventCopyWith<$Res> {
  _$FindActivityInputEventCopyWithImpl(this._value, this._then);

  final FindActivityInputEvent _value;
  // ignore: unused_field
  final $Res Function(FindActivityInputEvent) _then;
}

/// @nodoc
abstract class $FindActivityActivityTypeChangedCopyWith<$Res> {
  factory $FindActivityActivityTypeChangedCopyWith(
          FindActivityActivityTypeChanged value,
          $Res Function(FindActivityActivityTypeChanged) then) =
      _$FindActivityActivityTypeChangedCopyWithImpl<$Res>;
  $Res call({ActivityType activityType});
}

/// @nodoc
class _$FindActivityActivityTypeChangedCopyWithImpl<$Res>
    extends _$FindActivityInputEventCopyWithImpl<$Res>
    implements $FindActivityActivityTypeChangedCopyWith<$Res> {
  _$FindActivityActivityTypeChangedCopyWithImpl(
      FindActivityActivityTypeChanged _value,
      $Res Function(FindActivityActivityTypeChanged) _then)
      : super(_value, (v) => _then(v as FindActivityActivityTypeChanged));

  @override
  FindActivityActivityTypeChanged get _value =>
      super._value as FindActivityActivityTypeChanged;

  @override
  $Res call({
    Object? activityType = freezed,
  }) {
    return _then(FindActivityActivityTypeChanged(
      activityType == freezed
          ? _value.activityType
          : activityType // ignore: cast_nullable_to_non_nullable
              as ActivityType,
    ));
  }
}

/// @nodoc

class _$FindActivityActivityTypeChanged
    implements FindActivityActivityTypeChanged {
  const _$FindActivityActivityTypeChanged(this.activityType);

  @override
  final ActivityType activityType;

  @override
  String toString() {
    return 'FindActivityInputEvent.findActivityActivityTypeChanged(activityType: $activityType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FindActivityActivityTypeChanged &&
            const DeepCollectionEquality()
                .equals(other.activityType, activityType));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(activityType));

  @JsonKey(ignore: true)
  @override
  $FindActivityActivityTypeChangedCopyWith<FindActivityActivityTypeChanged>
      get copyWith => _$FindActivityActivityTypeChangedCopyWithImpl<
          FindActivityActivityTypeChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ActivityType activityType)
        findActivityActivityTypeChanged,
    required TResult Function(String participantsInput)
        findActivityParticipantsInputChanged,
  }) {
    return findActivityActivityTypeChanged(activityType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ActivityType activityType)?
        findActivityActivityTypeChanged,
    TResult Function(String participantsInput)?
        findActivityParticipantsInputChanged,
  }) {
    return findActivityActivityTypeChanged?.call(activityType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ActivityType activityType)?
        findActivityActivityTypeChanged,
    TResult Function(String participantsInput)?
        findActivityParticipantsInputChanged,
    required TResult orElse(),
  }) {
    if (findActivityActivityTypeChanged != null) {
      return findActivityActivityTypeChanged(activityType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FindActivityActivityTypeChanged value)
        findActivityActivityTypeChanged,
    required TResult Function(FindActivityParticipantsInputChanged value)
        findActivityParticipantsInputChanged,
  }) {
    return findActivityActivityTypeChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FindActivityActivityTypeChanged value)?
        findActivityActivityTypeChanged,
    TResult Function(FindActivityParticipantsInputChanged value)?
        findActivityParticipantsInputChanged,
  }) {
    return findActivityActivityTypeChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FindActivityActivityTypeChanged value)?
        findActivityActivityTypeChanged,
    TResult Function(FindActivityParticipantsInputChanged value)?
        findActivityParticipantsInputChanged,
    required TResult orElse(),
  }) {
    if (findActivityActivityTypeChanged != null) {
      return findActivityActivityTypeChanged(this);
    }
    return orElse();
  }
}

abstract class FindActivityActivityTypeChanged
    implements FindActivityInputEvent {
  const factory FindActivityActivityTypeChanged(ActivityType activityType) =
      _$FindActivityActivityTypeChanged;

  ActivityType get activityType;
  @JsonKey(ignore: true)
  $FindActivityActivityTypeChangedCopyWith<FindActivityActivityTypeChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivityParticipantsInputChangedCopyWith<$Res> {
  factory $FindActivityParticipantsInputChangedCopyWith(
          FindActivityParticipantsInputChanged value,
          $Res Function(FindActivityParticipantsInputChanged) then) =
      _$FindActivityParticipantsInputChangedCopyWithImpl<$Res>;
  $Res call({String participantsInput});
}

/// @nodoc
class _$FindActivityParticipantsInputChangedCopyWithImpl<$Res>
    extends _$FindActivityInputEventCopyWithImpl<$Res>
    implements $FindActivityParticipantsInputChangedCopyWith<$Res> {
  _$FindActivityParticipantsInputChangedCopyWithImpl(
      FindActivityParticipantsInputChanged _value,
      $Res Function(FindActivityParticipantsInputChanged) _then)
      : super(_value, (v) => _then(v as FindActivityParticipantsInputChanged));

  @override
  FindActivityParticipantsInputChanged get _value =>
      super._value as FindActivityParticipantsInputChanged;

  @override
  $Res call({
    Object? participantsInput = freezed,
  }) {
    return _then(FindActivityParticipantsInputChanged(
      participantsInput == freezed
          ? _value.participantsInput
          : participantsInput // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FindActivityParticipantsInputChanged
    implements FindActivityParticipantsInputChanged {
  const _$FindActivityParticipantsInputChanged(this.participantsInput);

  @override
  final String participantsInput;

  @override
  String toString() {
    return 'FindActivityInputEvent.findActivityParticipantsInputChanged(participantsInput: $participantsInput)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FindActivityParticipantsInputChanged &&
            const DeepCollectionEquality()
                .equals(other.participantsInput, participantsInput));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participantsInput));

  @JsonKey(ignore: true)
  @override
  $FindActivityParticipantsInputChangedCopyWith<
          FindActivityParticipantsInputChanged>
      get copyWith => _$FindActivityParticipantsInputChangedCopyWithImpl<
          FindActivityParticipantsInputChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ActivityType activityType)
        findActivityActivityTypeChanged,
    required TResult Function(String participantsInput)
        findActivityParticipantsInputChanged,
  }) {
    return findActivityParticipantsInputChanged(participantsInput);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ActivityType activityType)?
        findActivityActivityTypeChanged,
    TResult Function(String participantsInput)?
        findActivityParticipantsInputChanged,
  }) {
    return findActivityParticipantsInputChanged?.call(participantsInput);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ActivityType activityType)?
        findActivityActivityTypeChanged,
    TResult Function(String participantsInput)?
        findActivityParticipantsInputChanged,
    required TResult orElse(),
  }) {
    if (findActivityParticipantsInputChanged != null) {
      return findActivityParticipantsInputChanged(participantsInput);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FindActivityActivityTypeChanged value)
        findActivityActivityTypeChanged,
    required TResult Function(FindActivityParticipantsInputChanged value)
        findActivityParticipantsInputChanged,
  }) {
    return findActivityParticipantsInputChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FindActivityActivityTypeChanged value)?
        findActivityActivityTypeChanged,
    TResult Function(FindActivityParticipantsInputChanged value)?
        findActivityParticipantsInputChanged,
  }) {
    return findActivityParticipantsInputChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FindActivityActivityTypeChanged value)?
        findActivityActivityTypeChanged,
    TResult Function(FindActivityParticipantsInputChanged value)?
        findActivityParticipantsInputChanged,
    required TResult orElse(),
  }) {
    if (findActivityParticipantsInputChanged != null) {
      return findActivityParticipantsInputChanged(this);
    }
    return orElse();
  }
}

abstract class FindActivityParticipantsInputChanged
    implements FindActivityInputEvent {
  const factory FindActivityParticipantsInputChanged(String participantsInput) =
      _$FindActivityParticipantsInputChanged;

  String get participantsInput;
  @JsonKey(ignore: true)
  $FindActivityParticipantsInputChangedCopyWith<
          FindActivityParticipantsInputChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
class _$FindActivityInputStateTearOff {
  const _$FindActivityInputStateTearOff();

  _FindActivityInputState call(
      {required ActivityType selectedActivityType,
      required String selectedParticipants}) {
    return _FindActivityInputState(
      selectedActivityType: selectedActivityType,
      selectedParticipants: selectedParticipants,
    );
  }
}

/// @nodoc
const $FindActivityInputState = _$FindActivityInputStateTearOff();

/// @nodoc
mixin _$FindActivityInputState {
  ActivityType get selectedActivityType => throw _privateConstructorUsedError;
  String get selectedParticipants => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FindActivityInputStateCopyWith<FindActivityInputState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivityInputStateCopyWith<$Res> {
  factory $FindActivityInputStateCopyWith(FindActivityInputState value,
          $Res Function(FindActivityInputState) then) =
      _$FindActivityInputStateCopyWithImpl<$Res>;
  $Res call({ActivityType selectedActivityType, String selectedParticipants});
}

/// @nodoc
class _$FindActivityInputStateCopyWithImpl<$Res>
    implements $FindActivityInputStateCopyWith<$Res> {
  _$FindActivityInputStateCopyWithImpl(this._value, this._then);

  final FindActivityInputState _value;
  // ignore: unused_field
  final $Res Function(FindActivityInputState) _then;

  @override
  $Res call({
    Object? selectedActivityType = freezed,
    Object? selectedParticipants = freezed,
  }) {
    return _then(_value.copyWith(
      selectedActivityType: selectedActivityType == freezed
          ? _value.selectedActivityType
          : selectedActivityType // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      selectedParticipants: selectedParticipants == freezed
          ? _value.selectedParticipants
          : selectedParticipants // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$FindActivityInputStateCopyWith<$Res>
    implements $FindActivityInputStateCopyWith<$Res> {
  factory _$FindActivityInputStateCopyWith(_FindActivityInputState value,
          $Res Function(_FindActivityInputState) then) =
      __$FindActivityInputStateCopyWithImpl<$Res>;
  @override
  $Res call({ActivityType selectedActivityType, String selectedParticipants});
}

/// @nodoc
class __$FindActivityInputStateCopyWithImpl<$Res>
    extends _$FindActivityInputStateCopyWithImpl<$Res>
    implements _$FindActivityInputStateCopyWith<$Res> {
  __$FindActivityInputStateCopyWithImpl(_FindActivityInputState _value,
      $Res Function(_FindActivityInputState) _then)
      : super(_value, (v) => _then(v as _FindActivityInputState));

  @override
  _FindActivityInputState get _value => super._value as _FindActivityInputState;

  @override
  $Res call({
    Object? selectedActivityType = freezed,
    Object? selectedParticipants = freezed,
  }) {
    return _then(_FindActivityInputState(
      selectedActivityType: selectedActivityType == freezed
          ? _value.selectedActivityType
          : selectedActivityType // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      selectedParticipants: selectedParticipants == freezed
          ? _value.selectedParticipants
          : selectedParticipants // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FindActivityInputState implements _FindActivityInputState {
  const _$_FindActivityInputState(
      {required this.selectedActivityType, required this.selectedParticipants});

  @override
  final ActivityType selectedActivityType;
  @override
  final String selectedParticipants;

  @override
  String toString() {
    return 'FindActivityInputState(selectedActivityType: $selectedActivityType, selectedParticipants: $selectedParticipants)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FindActivityInputState &&
            const DeepCollectionEquality()
                .equals(other.selectedActivityType, selectedActivityType) &&
            const DeepCollectionEquality()
                .equals(other.selectedParticipants, selectedParticipants));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(selectedActivityType),
      const DeepCollectionEquality().hash(selectedParticipants));

  @JsonKey(ignore: true)
  @override
  _$FindActivityInputStateCopyWith<_FindActivityInputState> get copyWith =>
      __$FindActivityInputStateCopyWithImpl<_FindActivityInputState>(
          this, _$identity);
}

abstract class _FindActivityInputState implements FindActivityInputState {
  const factory _FindActivityInputState(
      {required ActivityType selectedActivityType,
      required String selectedParticipants}) = _$_FindActivityInputState;

  @override
  ActivityType get selectedActivityType;
  @override
  String get selectedParticipants;
  @override
  @JsonKey(ignore: true)
  _$FindActivityInputStateCopyWith<_FindActivityInputState> get copyWith =>
      throw _privateConstructorUsedError;
}
