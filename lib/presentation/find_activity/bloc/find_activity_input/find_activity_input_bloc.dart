import 'package:bloc/bloc.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'find_activity_input_bloc.freezed.dart';
part 'find_activity_input_event.dart';
part 'find_activity_input_state.dart';

@injectable
class FindActivityInputBloc extends Bloc<FindActivityInputEvent, FindActivityInputState> {
  FindActivityInputBloc()
      : super(const FindActivityInputState(
            selectedActivityType: ActivityType.recreational,
            selectedParticipants: '1')) {
    on<FindActivityActivityTypeChanged>((event, emit) =>
        emit(state.copyWith(selectedActivityType: event.activityType)));
    on<FindActivityParticipantsInputChanged>((event, emit) => emit(
        state.copyWith(selectedParticipants: event.participantsInput)));
  }
}
