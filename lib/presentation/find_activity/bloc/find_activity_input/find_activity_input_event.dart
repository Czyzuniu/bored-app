part of 'find_activity_input_bloc.dart';

@freezed
class FindActivityInputEvent with _$FindActivityInputEvent {
  const factory FindActivityInputEvent.findActivityActivityTypeChanged(
      ActivityType activityType) = FindActivityActivityTypeChanged;

  const factory FindActivityInputEvent.findActivityParticipantsInputChanged(
      String participantsInput) = FindActivityParticipantsInputChanged;
}
