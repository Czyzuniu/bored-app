part of 'find_activity_submit_bloc.dart';

@freezed
class FindActivitySubmitEvent with _$FindActivitySubmitEvent {
  const factory FindActivitySubmitEvent.findActivitySubmitPressed(
          ActivityType activityType, String participants) =
      FindActivitySubmitPressed;

  const factory FindActivitySubmitEvent.findActivitySubmitRandomPressed() =
      FindActivitySubmitRandomPressed;
}
