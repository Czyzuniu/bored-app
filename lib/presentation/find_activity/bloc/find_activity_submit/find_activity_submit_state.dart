part of 'find_activity_submit_bloc.dart';

@freezed
class FindActivitySubmitState with _$FindActivitySubmitState {
  const factory FindActivitySubmitState.initial() = _Initial;
  const factory FindActivitySubmitState.loaded(Activity activity) = _Loaded;
  const factory FindActivitySubmitState.loading() = _Loading;
  const factory FindActivitySubmitState.error(String message) = _Error;
}
