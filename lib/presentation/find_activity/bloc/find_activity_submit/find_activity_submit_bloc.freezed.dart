// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'find_activity_submit_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FindActivitySubmitEventTearOff {
  const _$FindActivitySubmitEventTearOff();

  FindActivitySubmitPressed findActivitySubmitPressed(
      ActivityType activityType, String participants) {
    return FindActivitySubmitPressed(
      activityType,
      participants,
    );
  }

  FindActivitySubmitRandomPressed findActivitySubmitRandomPressed() {
    return const FindActivitySubmitRandomPressed();
  }
}

/// @nodoc
const $FindActivitySubmitEvent = _$FindActivitySubmitEventTearOff();

/// @nodoc
mixin _$FindActivitySubmitEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ActivityType activityType, String participants)
        findActivitySubmitPressed,
    required TResult Function() findActivitySubmitRandomPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ActivityType activityType, String participants)?
        findActivitySubmitPressed,
    TResult Function()? findActivitySubmitRandomPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ActivityType activityType, String participants)?
        findActivitySubmitPressed,
    TResult Function()? findActivitySubmitRandomPressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FindActivitySubmitPressed value)
        findActivitySubmitPressed,
    required TResult Function(FindActivitySubmitRandomPressed value)
        findActivitySubmitRandomPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FindActivitySubmitPressed value)?
        findActivitySubmitPressed,
    TResult Function(FindActivitySubmitRandomPressed value)?
        findActivitySubmitRandomPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FindActivitySubmitPressed value)?
        findActivitySubmitPressed,
    TResult Function(FindActivitySubmitRandomPressed value)?
        findActivitySubmitRandomPressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivitySubmitEventCopyWith<$Res> {
  factory $FindActivitySubmitEventCopyWith(FindActivitySubmitEvent value,
          $Res Function(FindActivitySubmitEvent) then) =
      _$FindActivitySubmitEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$FindActivitySubmitEventCopyWithImpl<$Res>
    implements $FindActivitySubmitEventCopyWith<$Res> {
  _$FindActivitySubmitEventCopyWithImpl(this._value, this._then);

  final FindActivitySubmitEvent _value;
  // ignore: unused_field
  final $Res Function(FindActivitySubmitEvent) _then;
}

/// @nodoc
abstract class $FindActivitySubmitPressedCopyWith<$Res> {
  factory $FindActivitySubmitPressedCopyWith(FindActivitySubmitPressed value,
          $Res Function(FindActivitySubmitPressed) then) =
      _$FindActivitySubmitPressedCopyWithImpl<$Res>;
  $Res call({ActivityType activityType, String participants});
}

/// @nodoc
class _$FindActivitySubmitPressedCopyWithImpl<$Res>
    extends _$FindActivitySubmitEventCopyWithImpl<$Res>
    implements $FindActivitySubmitPressedCopyWith<$Res> {
  _$FindActivitySubmitPressedCopyWithImpl(FindActivitySubmitPressed _value,
      $Res Function(FindActivitySubmitPressed) _then)
      : super(_value, (v) => _then(v as FindActivitySubmitPressed));

  @override
  FindActivitySubmitPressed get _value =>
      super._value as FindActivitySubmitPressed;

  @override
  $Res call({
    Object? activityType = freezed,
    Object? participants = freezed,
  }) {
    return _then(FindActivitySubmitPressed(
      activityType == freezed
          ? _value.activityType
          : activityType // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FindActivitySubmitPressed implements FindActivitySubmitPressed {
  const _$FindActivitySubmitPressed(this.activityType, this.participants);

  @override
  final ActivityType activityType;
  @override
  final String participants;

  @override
  String toString() {
    return 'FindActivitySubmitEvent.findActivitySubmitPressed(activityType: $activityType, participants: $participants)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FindActivitySubmitPressed &&
            const DeepCollectionEquality()
                .equals(other.activityType, activityType) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(activityType),
      const DeepCollectionEquality().hash(participants));

  @JsonKey(ignore: true)
  @override
  $FindActivitySubmitPressedCopyWith<FindActivitySubmitPressed> get copyWith =>
      _$FindActivitySubmitPressedCopyWithImpl<FindActivitySubmitPressed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ActivityType activityType, String participants)
        findActivitySubmitPressed,
    required TResult Function() findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitPressed(activityType, participants);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ActivityType activityType, String participants)?
        findActivitySubmitPressed,
    TResult Function()? findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitPressed?.call(activityType, participants);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ActivityType activityType, String participants)?
        findActivitySubmitPressed,
    TResult Function()? findActivitySubmitRandomPressed,
    required TResult orElse(),
  }) {
    if (findActivitySubmitPressed != null) {
      return findActivitySubmitPressed(activityType, participants);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FindActivitySubmitPressed value)
        findActivitySubmitPressed,
    required TResult Function(FindActivitySubmitRandomPressed value)
        findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitPressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FindActivitySubmitPressed value)?
        findActivitySubmitPressed,
    TResult Function(FindActivitySubmitRandomPressed value)?
        findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitPressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FindActivitySubmitPressed value)?
        findActivitySubmitPressed,
    TResult Function(FindActivitySubmitRandomPressed value)?
        findActivitySubmitRandomPressed,
    required TResult orElse(),
  }) {
    if (findActivitySubmitPressed != null) {
      return findActivitySubmitPressed(this);
    }
    return orElse();
  }
}

abstract class FindActivitySubmitPressed implements FindActivitySubmitEvent {
  const factory FindActivitySubmitPressed(
          ActivityType activityType, String participants) =
      _$FindActivitySubmitPressed;

  ActivityType get activityType;
  String get participants;
  @JsonKey(ignore: true)
  $FindActivitySubmitPressedCopyWith<FindActivitySubmitPressed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivitySubmitRandomPressedCopyWith<$Res> {
  factory $FindActivitySubmitRandomPressedCopyWith(
          FindActivitySubmitRandomPressed value,
          $Res Function(FindActivitySubmitRandomPressed) then) =
      _$FindActivitySubmitRandomPressedCopyWithImpl<$Res>;
}

/// @nodoc
class _$FindActivitySubmitRandomPressedCopyWithImpl<$Res>
    extends _$FindActivitySubmitEventCopyWithImpl<$Res>
    implements $FindActivitySubmitRandomPressedCopyWith<$Res> {
  _$FindActivitySubmitRandomPressedCopyWithImpl(
      FindActivitySubmitRandomPressed _value,
      $Res Function(FindActivitySubmitRandomPressed) _then)
      : super(_value, (v) => _then(v as FindActivitySubmitRandomPressed));

  @override
  FindActivitySubmitRandomPressed get _value =>
      super._value as FindActivitySubmitRandomPressed;
}

/// @nodoc

class _$FindActivitySubmitRandomPressed
    implements FindActivitySubmitRandomPressed {
  const _$FindActivitySubmitRandomPressed();

  @override
  String toString() {
    return 'FindActivitySubmitEvent.findActivitySubmitRandomPressed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FindActivitySubmitRandomPressed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ActivityType activityType, String participants)
        findActivitySubmitPressed,
    required TResult Function() findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitRandomPressed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ActivityType activityType, String participants)?
        findActivitySubmitPressed,
    TResult Function()? findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitRandomPressed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ActivityType activityType, String participants)?
        findActivitySubmitPressed,
    TResult Function()? findActivitySubmitRandomPressed,
    required TResult orElse(),
  }) {
    if (findActivitySubmitRandomPressed != null) {
      return findActivitySubmitRandomPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FindActivitySubmitPressed value)
        findActivitySubmitPressed,
    required TResult Function(FindActivitySubmitRandomPressed value)
        findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitRandomPressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FindActivitySubmitPressed value)?
        findActivitySubmitPressed,
    TResult Function(FindActivitySubmitRandomPressed value)?
        findActivitySubmitRandomPressed,
  }) {
    return findActivitySubmitRandomPressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FindActivitySubmitPressed value)?
        findActivitySubmitPressed,
    TResult Function(FindActivitySubmitRandomPressed value)?
        findActivitySubmitRandomPressed,
    required TResult orElse(),
  }) {
    if (findActivitySubmitRandomPressed != null) {
      return findActivitySubmitRandomPressed(this);
    }
    return orElse();
  }
}

abstract class FindActivitySubmitRandomPressed
    implements FindActivitySubmitEvent {
  const factory FindActivitySubmitRandomPressed() =
      _$FindActivitySubmitRandomPressed;
}

/// @nodoc
class _$FindActivitySubmitStateTearOff {
  const _$FindActivitySubmitStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Loaded loaded(Activity activity) {
    return _Loaded(
      activity,
    );
  }

  _Loading loading() {
    return const _Loading();
  }

  _Error error(String message) {
    return _Error(
      message,
    );
  }
}

/// @nodoc
const $FindActivitySubmitState = _$FindActivitySubmitStateTearOff();

/// @nodoc
mixin _$FindActivitySubmitState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Activity activity) loaded,
    required TResult Function() loading,
    required TResult Function(String message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivitySubmitStateCopyWith<$Res> {
  factory $FindActivitySubmitStateCopyWith(FindActivitySubmitState value,
          $Res Function(FindActivitySubmitState) then) =
      _$FindActivitySubmitStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FindActivitySubmitStateCopyWithImpl<$Res>
    implements $FindActivitySubmitStateCopyWith<$Res> {
  _$FindActivitySubmitStateCopyWithImpl(this._value, this._then);

  final FindActivitySubmitState _value;
  // ignore: unused_field
  final $Res Function(FindActivitySubmitState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$FindActivitySubmitStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FindActivitySubmitState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Activity activity) loaded,
    required TResult Function() loading,
    required TResult Function(String message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FindActivitySubmitState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$LoadedCopyWith<$Res> {
  factory _$LoadedCopyWith(_Loaded value, $Res Function(_Loaded) then) =
      __$LoadedCopyWithImpl<$Res>;
  $Res call({Activity activity});

  $ActivityCopyWith<$Res> get activity;
}

/// @nodoc
class __$LoadedCopyWithImpl<$Res>
    extends _$FindActivitySubmitStateCopyWithImpl<$Res>
    implements _$LoadedCopyWith<$Res> {
  __$LoadedCopyWithImpl(_Loaded _value, $Res Function(_Loaded) _then)
      : super(_value, (v) => _then(v as _Loaded));

  @override
  _Loaded get _value => super._value as _Loaded;

  @override
  $Res call({
    Object? activity = freezed,
  }) {
    return _then(_Loaded(
      activity == freezed
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as Activity,
    ));
  }

  @override
  $ActivityCopyWith<$Res> get activity {
    return $ActivityCopyWith<$Res>(_value.activity, (value) {
      return _then(_value.copyWith(activity: value));
    });
  }
}

/// @nodoc

class _$_Loaded implements _Loaded {
  const _$_Loaded(this.activity);

  @override
  final Activity activity;

  @override
  String toString() {
    return 'FindActivitySubmitState.loaded(activity: $activity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Loaded &&
            const DeepCollectionEquality().equals(other.activity, activity));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(activity));

  @JsonKey(ignore: true)
  @override
  _$LoadedCopyWith<_Loaded> get copyWith =>
      __$LoadedCopyWithImpl<_Loaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Activity activity) loaded,
    required TResult Function() loading,
    required TResult Function(String message) error,
  }) {
    return loaded(activity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
  }) {
    return loaded?.call(activity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(activity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements FindActivitySubmitState {
  const factory _Loaded(Activity activity) = _$_Loaded;

  Activity get activity;
  @JsonKey(ignore: true)
  _$LoadedCopyWith<_Loaded> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res>
    extends _$FindActivitySubmitStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'FindActivitySubmitState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Activity activity) loaded,
    required TResult Function() loading,
    required TResult Function(String message) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements FindActivitySubmitState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$ErrorCopyWith<$Res> {
  factory _$ErrorCopyWith(_Error value, $Res Function(_Error) then) =
      __$ErrorCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$ErrorCopyWithImpl<$Res>
    extends _$FindActivitySubmitStateCopyWithImpl<$Res>
    implements _$ErrorCopyWith<$Res> {
  __$ErrorCopyWithImpl(_Error _value, $Res Function(_Error) _then)
      : super(_value, (v) => _then(v as _Error));

  @override
  _Error get _value => super._value as _Error;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_Error(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'FindActivitySubmitState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Error &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$ErrorCopyWith<_Error> get copyWith =>
      __$ErrorCopyWithImpl<_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Activity activity) loaded,
    required TResult Function() loading,
    required TResult Function(String message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Activity activity)? loaded,
    TResult Function()? loading,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements FindActivitySubmitState {
  const factory _Error(String message) = _$_Error;

  String get message;
  @JsonKey(ignore: true)
  _$ErrorCopyWith<_Error> get copyWith => throw _privateConstructorUsedError;
}
