import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:bored/domain/find_activity/usecases/find_activity.dart';
import 'package:bored/domain/find_activity/usecases/find_random_activity.dart';
import 'package:bored/domain/utils/input_converter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'find_activity_submit_bloc.freezed.dart';
part 'find_activity_submit_event.dart';
part 'find_activity_submit_state.dart';

@injectable
class FindActivitySubmitBloc
    extends Bloc<FindActivitySubmitEvent, FindActivitySubmitState> {
  final FindActivity findActivity;
  final FindRandomActivity findRandomActivity;
  final InputConverter inputConverter;

  FindActivitySubmitBloc(this.findActivity, this.inputConverter, this.findRandomActivity)
      : super(const FindActivitySubmitState.initial()) {
    on<FindActivitySubmitPressed>(_onFindActivitySubmitPressed);
    on<FindActivitySubmitRandomPressed>(_onFindActivitySubmitRandomPressed);
  }

  FutureOr<void> _onFindActivitySubmitPressed(FindActivitySubmitPressed event,
      Emitter<FindActivitySubmitState> emit) async {
    emit(const FindActivitySubmitState.loading());
    final inputConversionEither =
        inputConverter.stringToPositiveInteger(event.participants);
    await inputConversionEither.fold(
      (failure) {
        emit(FindActivitySubmitState.error(failure.message));
      },
      (participants) async {
        final activityEither = await findActivity.execute(SearchActivity(
            activityType: event.activityType, participants: participants));
        await activityEither.fold((failure) async {
          emit(FindActivitySubmitState.error(failure.message));
        }, (activity) async {
          emit(FindActivitySubmitState.loaded(activity));
        });
      },
    );
  }

  Future<FutureOr<void>> _onFindActivitySubmitRandomPressed(FindActivitySubmitRandomPressed event, Emitter<FindActivitySubmitState> emit) async {
    emit(const FindActivitySubmitState.loading());
    final activityEither = await findRandomActivity.execute();
    await activityEither.fold((failure) async {
      emit(FindActivitySubmitState.error(failure.message));
    }, (activity) async {
      emit(FindActivitySubmitState.loaded(activity));
    });
  }
}
