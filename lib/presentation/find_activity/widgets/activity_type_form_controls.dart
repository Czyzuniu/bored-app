import 'package:flutter/material.dart';

import 'find_activity_participants_input.dart';
import 'find_activity_submit_button.dart';
import 'find_activity_type_dropdown.dart';
import 'find_random_activity_submit_button.dart';

class FindActivityControls extends StatefulWidget {
  const FindActivityControls({
    Key? key,
  }) : super(key: key);

  @override
  State<FindActivityControls> createState() => _FindActivityControlsState();
}

class _FindActivityControlsState extends State<FindActivityControls> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children:  [
        const FindActivityTypeDropdown(),
        const FindActivityParticipantsInput(),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: const [
            FindActivitySubmitButton(),
            FindRandomActivitySubmitButton()
          ],
        ),
      ],
    );
  }
}
