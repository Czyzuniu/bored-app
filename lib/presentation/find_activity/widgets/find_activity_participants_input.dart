import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class FindActivityParticipantsInput extends StatelessWidget {
  const FindActivityParticipantsInput({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FindActivityInputBloc, FindActivityInputState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            children: [
              const Text(
                'For how many people?',
                style: TextStyle(),
                textAlign: TextAlign.left,
              ),
              TextFormField(
                key: const Key('ParticipantsInput'),
                textInputAction: TextInputAction.done,
                keyboardType: const TextInputType.numberWithOptions(signed: true, decimal: true),
                initialValue: state.selectedParticipants,
                onChanged: (val) => {
                  context
                      .read<FindActivityInputBloc>()
                      .add(FindActivityParticipantsInputChanged(val))
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
