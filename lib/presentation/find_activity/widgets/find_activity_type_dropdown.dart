import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class FindActivityTypeDropdown extends StatelessWidget {
  const FindActivityTypeDropdown({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FindActivityInputBloc, FindActivityInputState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            children: [
              const Text(
                'What kind of activity?',
                style: TextStyle(),
                textAlign: TextAlign.left,
              ),
              DropdownButtonFormField<ActivityType>(
                key: const Key('ActivityTypeDropdown'),
                value: state.selectedActivityType,
                icon: const Icon(Icons.arrow_downward),
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                onChanged: (ActivityType? newValue) {
                  context
                      .read<FindActivityInputBloc>()
                      .add(FindActivityActivityTypeChanged(newValue!));
                },
                items: ActivityType.values
                    .map<DropdownMenuItem<ActivityType>>((ActivityType value) {
                  return DropdownMenuItem<ActivityType>(
                    value: value,
                    child: Text(value.name),
                  );
                }).toList(),
              ),
            ],
          ),
        );
      },
    );
  }
}
