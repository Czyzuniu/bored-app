import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FindActivitySubmitButton extends StatelessWidget {
  const FindActivitySubmitButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FindActivitySubmitBloc, FindActivitySubmitState>(
      builder: (context, state) {
        return ElevatedButton.icon(
          key: const Key('SubmitFindActivityButton'),
          onPressed: () {
            state.maybeWhen(
                loading: () => null,
                orElse: () {
                  _onSubmitPressed(context);
                });
          },
          style: ButtonStyle(
              textStyle:
                  MaterialStateProperty.all(const TextStyle(fontSize: 20))),
          icon: const Icon(
            Icons.search,
            color: Colors.white,
            size: 25.0,
          ),
          label: const Text('Get me a specific activity!'),
        );
      },
    );
  }

  void _onSubmitPressed(BuildContext context) {
    FindActivityInputBloc findActivityBloc =
        context.read<FindActivityInputBloc>();
    FindActivitySubmitBloc findActivitySubmitBloc =
        context.read<FindActivitySubmitBloc>();

    findActivitySubmitBloc.add(
        FindActivitySubmitEvent.findActivitySubmitPressed(
            findActivityBloc.state.selectedActivityType,
            findActivityBloc.state.selectedParticipants));
  }
}
