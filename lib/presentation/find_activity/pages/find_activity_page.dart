import 'package:bored/presentation/app_drawer.dart';
import 'package:bored/presentation/core/loading_widget.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart';
import 'package:bored/presentation/find_activity/widgets/activity_type_form_controls.dart';
import 'package:bored/presentation/find_activity/widgets/message_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FindActivityPage extends StatelessWidget {
  const FindActivityPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Do Something!'),
      ),
      drawer: const AppDrawer(),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              BlocBuilder<FindActivitySubmitBloc, FindActivitySubmitState>(
                builder: (context, state) {
                  return Column(
                    children: [
                      _renderBasedOnState(state),
                      const SizedBox(
                        height: 200,
                      ),
                      const FindActivityControls(),
                    ],
                  );
                },
              ),
              // Bottom half
            ],
          ),
        ),
      ),
    );
  }

  Widget _renderBasedOnState(FindActivitySubmitState state) {
    return state.when(
        initial: () => const MessageDisplay(
            message: 'Start Searching!', key: Key('Initial')),
        loaded: (activity) => MessageDisplay(
            message: activity.activity, key: const Key('Loaded')),
        loading: () => const LoadingWidget(key: Key('Loading')),
        error: (message) =>
            MessageDisplay(message: message, key: const Key('Error')));
  }
}
