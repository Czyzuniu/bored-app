import 'package:bored/dependency_injection/dependency_container.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart';
import 'package:bored/presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'find_activity_page.dart';

class FindActivityPageView extends StatelessWidget {
  const FindActivityPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<FindActivityInputBloc>(
          create: (BuildContext context) => getIt<FindActivityInputBloc>(),
        ),
        BlocProvider<FindActivitySubmitBloc>(
          create: (BuildContext context) => getIt<FindActivitySubmitBloc>(),
        )
      ],
      child: const FindActivityPage(),
    );
  }
}
