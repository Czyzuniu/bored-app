import 'package:bored/presentation/find_activity/pages/find_activity_page_view.dart';
import 'package:bored/presentation/weather/pages/weather_page_view.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(''),
          ),
          ListTile(
            title: const Text('Find Activity'),
            onTap: () {
              Navigator.push<FindActivityPageView>(
                context,
                MaterialPageRoute(
                    builder: (context) => const FindActivityPageView()),
              );
            },
          ),
          ListTile(
            title: const Text('Weather'),
            onTap: () {
              Navigator.push<WeatherPageView>(context,
                  MaterialPageRoute(builder: (context) => const WeatherPageView()));
            },
          ),
        ],
      ),
    );
  }
}
