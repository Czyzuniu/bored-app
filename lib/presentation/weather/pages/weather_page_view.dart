import 'package:bored/dependency_injection/dependency_container.dart';
import 'package:bored/presentation/weather/cubit/weather_cubit.dart';
import 'package:bored/presentation/weather/pages/weather_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherPageView extends StatelessWidget {
  const WeatherPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => getIt<WeatherCubit>()..getWeather(),
        child: const WeatherPage());
  }
}
