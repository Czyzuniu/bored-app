import 'package:bored/presentation/app_drawer.dart';
import 'package:bored/presentation/core/loading_widget.dart';
import 'package:bored/presentation/weather/cubit/weather_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Weather'),
        ),
        drawer: const AppDrawer(),
        body: Center(child: BlocBuilder<WeatherCubit, WeatherState>(
          builder: (context, state) {
            print(state);
            return state.maybeWhen(loaded: (weather) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Current Temperature'),
                  Text(weather.celcius.toString() + ' Celsius'),
                ],
              );
            }, error: (message) {
              return Center(
                child: Text(message),
              );
            }, orElse: () {
              return const LoadingWidget(
                key: Key('Loading'),
              );
            });
          },
        )));
  }
}
