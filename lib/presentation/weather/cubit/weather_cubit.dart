import 'package:bloc/bloc.dart';
import 'package:bored/domain/weather/entities/weather.dart';
import 'package:bored/domain/weather/usecase/get_weather_by_localization.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'weather_cubit.freezed.dart';
part 'weather_state.dart';

@LazySingleton()
class WeatherCubit extends Cubit<WeatherState> {
  final GetWeatherByLocalization getWeatherByLocalization;

  WeatherCubit(this.getWeatherByLocalization)
      : super(const WeatherState.initial());

  void getWeather() async {
    emit(const WeatherState.loading());
    final result = await getWeatherByLocalization.execute();

    result.fold((l) => emit(WeatherState.error(l.message)),
        (r) => emit(WeatherState.loaded(r)));
  }
}
