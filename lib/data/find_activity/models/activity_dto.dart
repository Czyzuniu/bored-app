import 'package:bored/domain/find_activity/entities/activity_type.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'activity_dto.freezed.dart';
part 'activity_dto.g.dart';

@freezed
class ActivityDto with _$ActivityDto {
  @JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
  const factory ActivityDto({
    required String activity,
    required ActivityType type,
    required int participants
  }) = _ActivityDto;


  factory ActivityDto.fromJson(Map<String, dynamic> json) =>
      _$ActivityDtoFromJson(json);
}