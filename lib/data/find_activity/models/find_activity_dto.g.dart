// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'find_activity_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FindActivityDto _$$_FindActivityDtoFromJson(Map<String, dynamic> json) =>
    _$_FindActivityDto(
      type: json['type'] as String,
      participants: json['participants'] as String,
    );

Map<String, dynamic> _$$_FindActivityDtoToJson(_$_FindActivityDto instance) =>
    <String, dynamic>{
      'type': instance.type,
      'participants': instance.participants,
    };
