// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'activity_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ActivityDto _$ActivityDtoFromJson(Map<String, dynamic> json) {
  return _ActivityDto.fromJson(json);
}

/// @nodoc
class _$ActivityDtoTearOff {
  const _$ActivityDtoTearOff();

  _ActivityDto call(
      {required String activity,
      required ActivityType type,
      required int participants}) {
    return _ActivityDto(
      activity: activity,
      type: type,
      participants: participants,
    );
  }

  ActivityDto fromJson(Map<String, Object?> json) {
    return ActivityDto.fromJson(json);
  }
}

/// @nodoc
const $ActivityDto = _$ActivityDtoTearOff();

/// @nodoc
mixin _$ActivityDto {
  String get activity => throw _privateConstructorUsedError;
  ActivityType get type => throw _privateConstructorUsedError;
  int get participants => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ActivityDtoCopyWith<ActivityDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityDtoCopyWith<$Res> {
  factory $ActivityDtoCopyWith(
          ActivityDto value, $Res Function(ActivityDto) then) =
      _$ActivityDtoCopyWithImpl<$Res>;
  $Res call({String activity, ActivityType type, int participants});
}

/// @nodoc
class _$ActivityDtoCopyWithImpl<$Res> implements $ActivityDtoCopyWith<$Res> {
  _$ActivityDtoCopyWithImpl(this._value, this._then);

  final ActivityDto _value;
  // ignore: unused_field
  final $Res Function(ActivityDto) _then;

  @override
  $Res call({
    Object? activity = freezed,
    Object? type = freezed,
    Object? participants = freezed,
  }) {
    return _then(_value.copyWith(
      activity: activity == freezed
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as String,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$ActivityDtoCopyWith<$Res>
    implements $ActivityDtoCopyWith<$Res> {
  factory _$ActivityDtoCopyWith(
          _ActivityDto value, $Res Function(_ActivityDto) then) =
      __$ActivityDtoCopyWithImpl<$Res>;
  @override
  $Res call({String activity, ActivityType type, int participants});
}

/// @nodoc
class __$ActivityDtoCopyWithImpl<$Res> extends _$ActivityDtoCopyWithImpl<$Res>
    implements _$ActivityDtoCopyWith<$Res> {
  __$ActivityDtoCopyWithImpl(
      _ActivityDto _value, $Res Function(_ActivityDto) _then)
      : super(_value, (v) => _then(v as _ActivityDto));

  @override
  _ActivityDto get _value => super._value as _ActivityDto;

  @override
  $Res call({
    Object? activity = freezed,
    Object? type = freezed,
    Object? participants = freezed,
  }) {
    return _then(_ActivityDto(
      activity: activity == freezed
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as String,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
class _$_ActivityDto with DiagnosticableTreeMixin implements _ActivityDto {
  const _$_ActivityDto(
      {required this.activity, required this.type, required this.participants});

  factory _$_ActivityDto.fromJson(Map<String, dynamic> json) =>
      _$$_ActivityDtoFromJson(json);

  @override
  final String activity;
  @override
  final ActivityType type;
  @override
  final int participants;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ActivityDto(activity: $activity, type: $type, participants: $participants)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ActivityDto'))
      ..add(DiagnosticsProperty('activity', activity))
      ..add(DiagnosticsProperty('type', type))
      ..add(DiagnosticsProperty('participants', participants));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ActivityDto &&
            const DeepCollectionEquality().equals(other.activity, activity) &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(activity),
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(participants));

  @JsonKey(ignore: true)
  @override
  _$ActivityDtoCopyWith<_ActivityDto> get copyWith =>
      __$ActivityDtoCopyWithImpl<_ActivityDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ActivityDtoToJson(this);
  }
}

abstract class _ActivityDto implements ActivityDto {
  const factory _ActivityDto(
      {required String activity,
      required ActivityType type,
      required int participants}) = _$_ActivityDto;

  factory _ActivityDto.fromJson(Map<String, dynamic> json) =
      _$_ActivityDto.fromJson;

  @override
  String get activity;
  @override
  ActivityType get type;
  @override
  int get participants;
  @override
  @JsonKey(ignore: true)
  _$ActivityDtoCopyWith<_ActivityDto> get copyWith =>
      throw _privateConstructorUsedError;
}
