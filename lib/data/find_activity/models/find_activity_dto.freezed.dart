// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'find_activity_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FindActivityDto _$FindActivityDtoFromJson(Map<String, dynamic> json) {
  return _FindActivityDto.fromJson(json);
}

/// @nodoc
class _$FindActivityDtoTearOff {
  const _$FindActivityDtoTearOff();

  _FindActivityDto call({required String type, required String participants}) {
    return _FindActivityDto(
      type: type,
      participants: participants,
    );
  }

  FindActivityDto fromJson(Map<String, Object?> json) {
    return FindActivityDto.fromJson(json);
  }
}

/// @nodoc
const $FindActivityDto = _$FindActivityDtoTearOff();

/// @nodoc
mixin _$FindActivityDto {
  String get type => throw _privateConstructorUsedError;
  String get participants => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FindActivityDtoCopyWith<FindActivityDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FindActivityDtoCopyWith<$Res> {
  factory $FindActivityDtoCopyWith(
          FindActivityDto value, $Res Function(FindActivityDto) then) =
      _$FindActivityDtoCopyWithImpl<$Res>;
  $Res call({String type, String participants});
}

/// @nodoc
class _$FindActivityDtoCopyWithImpl<$Res>
    implements $FindActivityDtoCopyWith<$Res> {
  _$FindActivityDtoCopyWithImpl(this._value, this._then);

  final FindActivityDto _value;
  // ignore: unused_field
  final $Res Function(FindActivityDto) _then;

  @override
  $Res call({
    Object? type = freezed,
    Object? participants = freezed,
  }) {
    return _then(_value.copyWith(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$FindActivityDtoCopyWith<$Res>
    implements $FindActivityDtoCopyWith<$Res> {
  factory _$FindActivityDtoCopyWith(
          _FindActivityDto value, $Res Function(_FindActivityDto) then) =
      __$FindActivityDtoCopyWithImpl<$Res>;
  @override
  $Res call({String type, String participants});
}

/// @nodoc
class __$FindActivityDtoCopyWithImpl<$Res>
    extends _$FindActivityDtoCopyWithImpl<$Res>
    implements _$FindActivityDtoCopyWith<$Res> {
  __$FindActivityDtoCopyWithImpl(
      _FindActivityDto _value, $Res Function(_FindActivityDto) _then)
      : super(_value, (v) => _then(v as _FindActivityDto));

  @override
  _FindActivityDto get _value => super._value as _FindActivityDto;

  @override
  $Res call({
    Object? type = freezed,
    Object? participants = freezed,
  }) {
    return _then(_FindActivityDto(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
class _$_FindActivityDto
    with DiagnosticableTreeMixin
    implements _FindActivityDto {
  const _$_FindActivityDto({required this.type, required this.participants});

  factory _$_FindActivityDto.fromJson(Map<String, dynamic> json) =>
      _$$_FindActivityDtoFromJson(json);

  @override
  final String type;
  @override
  final String participants;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FindActivityDto(type: $type, participants: $participants)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FindActivityDto'))
      ..add(DiagnosticsProperty('type', type))
      ..add(DiagnosticsProperty('participants', participants));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FindActivityDto &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(participants));

  @JsonKey(ignore: true)
  @override
  _$FindActivityDtoCopyWith<_FindActivityDto> get copyWith =>
      __$FindActivityDtoCopyWithImpl<_FindActivityDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FindActivityDtoToJson(this);
  }
}

abstract class _FindActivityDto implements FindActivityDto {
  const factory _FindActivityDto(
      {required String type,
      required String participants}) = _$_FindActivityDto;

  factory _FindActivityDto.fromJson(Map<String, dynamic> json) =
      _$_FindActivityDto.fromJson;

  @override
  String get type;
  @override
  String get participants;
  @override
  @JsonKey(ignore: true)
  _$FindActivityDtoCopyWith<_FindActivityDto> get copyWith =>
      throw _privateConstructorUsedError;
}
