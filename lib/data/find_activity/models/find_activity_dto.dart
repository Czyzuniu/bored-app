import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'find_activity_dto.freezed.dart';
part 'find_activity_dto.g.dart';

@freezed
class FindActivityDto with _$FindActivityDto {
  @JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
  const factory FindActivityDto({
    required String type,
    required String participants
  }) = _FindActivityDto;



  factory FindActivityDto.fromJson(Map<String, dynamic> json) =>
      _$FindActivityDtoFromJson(json);
}