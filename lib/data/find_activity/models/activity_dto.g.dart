// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ActivityDto _$$_ActivityDtoFromJson(Map<String, dynamic> json) =>
    _$_ActivityDto(
      activity: json['activity'] as String,
      type: $enumDecode(_$ActivityTypeEnumMap, json['type']),
      participants: json['participants'] as int,
    );

Map<String, dynamic> _$$_ActivityDtoToJson(_$_ActivityDto instance) =>
    <String, dynamic>{
      'activity': instance.activity,
      'type': _$ActivityTypeEnumMap[instance.type],
      'participants': instance.participants,
    };

const _$ActivityTypeEnumMap = {
  ActivityType.recreational: 'recreational',
  ActivityType.education: 'education',
  ActivityType.social: 'social',
  ActivityType.relaxation: 'relaxation',
  ActivityType.cooking: 'cooking',
  ActivityType.diy: 'diy',
  ActivityType.music: 'music',
  ActivityType.busywork: 'busywork',
  ActivityType.charity: 'charity',
};
