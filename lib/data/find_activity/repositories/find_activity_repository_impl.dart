import 'package:bored/data/find_activity/datasources/find_activity_remote_data_source.dart';
import 'package:bored/data/find_activity/mapper/activity_dto_mapper.dart';
import 'package:bored/data/find_activity/mapper/search_activity_dto_mapper.dart';
import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/core/repositories/get_network_info_repository.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:bored/domain/find_activity/repositories/find_activity_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: FindActivityRepository)
class FindActivityRepositoryImpl implements FindActivityRepository {
  final FindActivityRemoteDataSource remoteDataSource;
  final GetNetworkInfoRepository networkInfo;
  final ActivityDtoMapper activityDtoMapper;
  final SearchActivityMapper searchActivityMapper;

  FindActivityRepositoryImpl(
      {required this.remoteDataSource,
      required this.networkInfo,
      required this.activityDtoMapper,
      required this.searchActivityMapper});

  @override
  Future<Either<Failure, Activity>> findActivity(
      SearchActivity searchActivity) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(activityDtoMapper.toActivity(
            await remoteDataSource.findActivity(
                searchActivityMapper.toFindActivityDto(searchActivity))));
      } on ServerException catch (e) {
        return Left(Failure.server(e.message));
      } on NoRecordException catch (e) {
        return Left(Failure.noActivityFound(e.message));
      }
    }

    return const Left(Failure.noInternet('There is no internet.'));
  }

  @override
  Future<Either<Failure, Activity>> findRandomActivity() async {
    if (await networkInfo.isConnected) {
      try {
        return Right(activityDtoMapper
            .toActivity(await remoteDataSource.findAnyRandomActivity()));
      } on ServerException catch (e) {
        return Left(Failure.server(e.message));
      } on NoRecordException catch (e) {
        return Left(Failure.noActivityFound(e.message));
      }
    }

    return const Left(Failure.noInternet('There is no internet.'));
  }
}
