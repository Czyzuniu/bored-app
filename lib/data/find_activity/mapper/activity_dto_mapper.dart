import 'package:bored/data/find_activity/models/activity_dto.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:injectable/injectable.dart';

abstract class ActivityDtoMapper {
  Activity toActivity(ActivityDto activityDto);
}

@LazySingleton(as: ActivityDtoMapper)
class ActivityDtoMapperImpl implements ActivityDtoMapper {
  @override
  Activity toActivity(ActivityDto activityDto) {
    return Activity(
        activity: activityDto.activity,
        activityType: activityDto.type,
        participants: activityDto.participants
    );
  }
}