import 'package:bored/data/find_activity/models/find_activity_dto.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:injectable/injectable.dart';

abstract class SearchActivityMapper {
  FindActivityDto toFindActivityDto(SearchActivity searchActivity);
}

@LazySingleton(as: SearchActivityMapper)
class SearchActivityMapperImpl implements SearchActivityMapper {
  @override
  FindActivityDto toFindActivityDto(SearchActivity searchActivity) {
    return FindActivityDto(
        type: searchActivity.activityType.name,
        participants: searchActivity.participants.toString());
  }
}