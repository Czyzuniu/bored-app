import 'dart:convert';

import 'package:bored/data/find_activity/models/activity_dto.dart';
import 'package:bored/data/find_activity/models/find_activity_dto.dart';
import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';


abstract class FindActivityRemoteDataSource {
  Future<ActivityDto> findActivity(FindActivityDto dto);
  Future<ActivityDto> findAnyRandomActivity();
}

@LazySingleton(as: FindActivityRemoteDataSource)
class FindActivityRemoteDataSourceImpl implements FindActivityRemoteDataSource {
  final http.Client client;
  final String baseApiUrl = 'www.boredapi.com';
  final String baseApiPath = '/api/activity';

  FindActivityRemoteDataSourceImpl({required this.client});

  @override
  Future<ActivityDto> findActivity(FindActivityDto findActivityDto) {
    return _getActivity(findActivityDto);
  }

  Future<ActivityDto> _getActivity(FindActivityDto? findActivityDto) async {
    http.Response response = await fetchActivity(findActivityDto?.toJson());
    if (response.statusCode == 200) {
      try {
        return ActivityDto.fromJson(jsonDecode(response.body) as Map<String, dynamic>);
      } catch (e) {
        throw NoRecordException('No activity found with the specified parameters');
      }
    } else {
      throw ServerException('Server Failure');
    }
  }

  Future<http.Response> fetchActivity(Map<String, dynamic>? queryParameters) async {
    final response = await client.get(
      Uri.https(baseApiUrl, baseApiPath, queryParameters),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    return response;
  }

  @override
  Future<ActivityDto> findAnyRandomActivity() {
    return _getActivity(null);
  }
  
}