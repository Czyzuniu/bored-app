import 'package:bored/data/weather/datasources/get_weather_remote_data_source.dart';
import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/weather/entities/weather.dart';
import 'package:bored/domain/weather/repositories/get_weather_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: GetWeatherRepository)
class GetWeatherRepositoryImpl implements GetWeatherRepository {
  final GetWeatherRemoteDataSource getWeatherRemoteDataSource;

  GetWeatherRepositoryImpl(this.getWeatherRemoteDataSource);

  @override
  Future<Either<Failure, Weather>> getWeatherByCoordinate(
      CoordinativeLocation coords) async {
    try {
      final result =
          await getWeatherRemoteDataSource.getWeatherByLatAndLong(coords);
      return Right(Weather(celcius: result.weatherDto.temperature));
    } on ServerException catch (e) {
      return Left(Failure.server(e.message));
    } on NoRecordException catch (e) {
      return Left(Failure.noActivityFound(e.message));
    }
  }
}
