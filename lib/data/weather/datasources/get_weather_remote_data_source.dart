import 'dart:convert';

import 'package:bored/data/weather/models/fetch_weather_parameters_dto.dart';
import 'package:bored/data/weather/models/get_weather_dto.dart';
import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/core/errors/exceptions.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

abstract class GetWeatherRemoteDataSource {
  Future<GetWeatherDto> getWeatherByLatAndLong(CoordinativeLocation location);
}

@LazySingleton(as: GetWeatherRemoteDataSource)
class GetWeatherRemoteDataSourceImpl implements GetWeatherRemoteDataSource {
  final http.Client client;
  final String baseApiUrl = 'api.open-meteo.com';
  final String baseApiPath = '/v1/forecast';

  GetWeatherRemoteDataSourceImpl(this.client);

  @override
  Future<GetWeatherDto> getWeatherByLatAndLong(
      CoordinativeLocation location) async {
    return _getWeather(FetchWeatherParametersDto(
      location.getLatitudeAsString(),
      location.getLongitudeAsString(),
    ));
  }

  Future<http.Response> getWeather(
      Map<String, dynamic>? queryParameters) async {
    final response = await client.get(
      Uri.https(baseApiUrl, baseApiPath, queryParameters),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    return response;
  }

  Future<GetWeatherDto> _getWeather(
      FetchWeatherParametersDto parametersDto) async {
    http.Response response = await getWeather(parametersDto.toJson());
    if (response.statusCode == 200) {
      try {
        return GetWeatherDto.fromJson(
            jsonDecode(response.body) as Map<String, dynamic>);
      } catch (e) {
        throw NoRecordException('Could not fetch the weather');
      }
    } else {
      throw ServerException('Server Failure');
    }
  }
}
