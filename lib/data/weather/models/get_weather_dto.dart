import 'package:bored/data/weather/models/weather_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'get_weather_dto.freezed.dart';

part 'get_weather_dto.g.dart';

@freezed
class GetWeatherDto with _$GetWeatherDto {
  @JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
  factory GetWeatherDto(@JsonKey(name: 'current_weather') WeatherDto weatherDto,
      double latitude, double longitude) = _GetWeatherDto;

  factory GetWeatherDto.fromJson(Map<String, dynamic> json) =>
      _$GetWeatherDtoFromJson(json);
}
