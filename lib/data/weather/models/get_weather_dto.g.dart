// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_weather_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_GetWeatherDto _$$_GetWeatherDtoFromJson(Map<String, dynamic> json) =>
    _$_GetWeatherDto(
      WeatherDto.fromJson(json['current_weather'] as Map<String, dynamic>),
      (json['latitude'] as num).toDouble(),
      (json['longitude'] as num).toDouble(),
    );

Map<String, dynamic> _$$_GetWeatherDtoToJson(_$_GetWeatherDto instance) =>
    <String, dynamic>{
      'current_weather': instance.weatherDto.toJson(),
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
