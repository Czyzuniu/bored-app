// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WeatherDto _$$_WeatherDtoFromJson(Map<String, dynamic> json) =>
    _$_WeatherDto(
      (json['temperature'] as num).toDouble(),
      json['weathercode'] as int,
      (json['windspeed'] as num).toDouble(),
      (json['winddirection'] as num).toDouble(),
    );

Map<String, dynamic> _$$_WeatherDtoToJson(_$_WeatherDto instance) =>
    <String, dynamic>{
      'temperature': instance.temperature,
      'weathercode': instance.weatherCode,
      'windspeed': instance.windSpeed,
      'winddirection': instance.windDirection,
    };
