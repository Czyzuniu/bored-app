// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'fetch_weather_parameters_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FetchWeatherParametersDto _$FetchWeatherParametersDtoFromJson(
    Map<String, dynamic> json) {
  return _FetchWeatherParametersDto.fromJson(json);
}

/// @nodoc
class _$FetchWeatherParametersDtoTearOff {
  const _$FetchWeatherParametersDtoTearOff();

  _FetchWeatherParametersDto call(
      String latitude,
      String longitude,
      [@JsonKey(name: 'current_weather', defaultValue: 'true')
          String displayCurrentWeather = 'true']) {
    return _FetchWeatherParametersDto(
      latitude,
      longitude,
      displayCurrentWeather,
    );
  }

  FetchWeatherParametersDto fromJson(Map<String, Object?> json) {
    return FetchWeatherParametersDto.fromJson(json);
  }
}

/// @nodoc
const $FetchWeatherParametersDto = _$FetchWeatherParametersDtoTearOff();

/// @nodoc
mixin _$FetchWeatherParametersDto {
  String get latitude => throw _privateConstructorUsedError;
  String get longitude => throw _privateConstructorUsedError;
  @JsonKey(name: 'current_weather', defaultValue: 'true')
  String get displayCurrentWeather => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FetchWeatherParametersDtoCopyWith<FetchWeatherParametersDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchWeatherParametersDtoCopyWith<$Res> {
  factory $FetchWeatherParametersDtoCopyWith(FetchWeatherParametersDto value,
          $Res Function(FetchWeatherParametersDto) then) =
      _$FetchWeatherParametersDtoCopyWithImpl<$Res>;
  $Res call(
      {String latitude,
      String longitude,
      @JsonKey(name: 'current_weather', defaultValue: 'true')
          String displayCurrentWeather});
}

/// @nodoc
class _$FetchWeatherParametersDtoCopyWithImpl<$Res>
    implements $FetchWeatherParametersDtoCopyWith<$Res> {
  _$FetchWeatherParametersDtoCopyWithImpl(this._value, this._then);

  final FetchWeatherParametersDto _value;
  // ignore: unused_field
  final $Res Function(FetchWeatherParametersDto) _then;

  @override
  $Res call({
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? displayCurrentWeather = freezed,
  }) {
    return _then(_value.copyWith(
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as String,
      displayCurrentWeather: displayCurrentWeather == freezed
          ? _value.displayCurrentWeather
          : displayCurrentWeather // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$FetchWeatherParametersDtoCopyWith<$Res>
    implements $FetchWeatherParametersDtoCopyWith<$Res> {
  factory _$FetchWeatherParametersDtoCopyWith(_FetchWeatherParametersDto value,
          $Res Function(_FetchWeatherParametersDto) then) =
      __$FetchWeatherParametersDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String latitude,
      String longitude,
      @JsonKey(name: 'current_weather', defaultValue: 'true')
          String displayCurrentWeather});
}

/// @nodoc
class __$FetchWeatherParametersDtoCopyWithImpl<$Res>
    extends _$FetchWeatherParametersDtoCopyWithImpl<$Res>
    implements _$FetchWeatherParametersDtoCopyWith<$Res> {
  __$FetchWeatherParametersDtoCopyWithImpl(_FetchWeatherParametersDto _value,
      $Res Function(_FetchWeatherParametersDto) _then)
      : super(_value, (v) => _then(v as _FetchWeatherParametersDto));

  @override
  _FetchWeatherParametersDto get _value =>
      super._value as _FetchWeatherParametersDto;

  @override
  $Res call({
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? displayCurrentWeather = freezed,
  }) {
    return _then(_FetchWeatherParametersDto(
      latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as String,
      longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as String,
      displayCurrentWeather == freezed
          ? _value.displayCurrentWeather
          : displayCurrentWeather // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
class _$_FetchWeatherParametersDto implements _FetchWeatherParametersDto {
  const _$_FetchWeatherParametersDto(
      this.latitude,
      this.longitude,
      [@JsonKey(name: 'current_weather', defaultValue: 'true')
          this.displayCurrentWeather = 'true']);

  factory _$_FetchWeatherParametersDto.fromJson(Map<String, dynamic> json) =>
      _$$_FetchWeatherParametersDtoFromJson(json);

  @override
  final String latitude;
  @override
  final String longitude;
  @override
  @JsonKey(name: 'current_weather', defaultValue: 'true')
  final String displayCurrentWeather;

  @override
  String toString() {
    return 'FetchWeatherParametersDto(latitude: $latitude, longitude: $longitude, displayCurrentWeather: $displayCurrentWeather)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FetchWeatherParametersDto &&
            const DeepCollectionEquality().equals(other.latitude, latitude) &&
            const DeepCollectionEquality().equals(other.longitude, longitude) &&
            const DeepCollectionEquality()
                .equals(other.displayCurrentWeather, displayCurrentWeather));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(latitude),
      const DeepCollectionEquality().hash(longitude),
      const DeepCollectionEquality().hash(displayCurrentWeather));

  @JsonKey(ignore: true)
  @override
  _$FetchWeatherParametersDtoCopyWith<_FetchWeatherParametersDto>
      get copyWith =>
          __$FetchWeatherParametersDtoCopyWithImpl<_FetchWeatherParametersDto>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FetchWeatherParametersDtoToJson(this);
  }
}

abstract class _FetchWeatherParametersDto implements FetchWeatherParametersDto {
  const factory _FetchWeatherParametersDto(
      String latitude,
      String longitude,
      [@JsonKey(name: 'current_weather', defaultValue: 'true')
          String displayCurrentWeather]) = _$_FetchWeatherParametersDto;

  factory _FetchWeatherParametersDto.fromJson(Map<String, dynamic> json) =
      _$_FetchWeatherParametersDto.fromJson;

  @override
  String get latitude;
  @override
  String get longitude;
  @override
  @JsonKey(name: 'current_weather', defaultValue: 'true')
  String get displayCurrentWeather;
  @override
  @JsonKey(ignore: true)
  _$FetchWeatherParametersDtoCopyWith<_FetchWeatherParametersDto>
      get copyWith => throw _privateConstructorUsedError;
}
