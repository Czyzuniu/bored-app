// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_weather_parameters_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FetchWeatherParametersDto _$$_FetchWeatherParametersDtoFromJson(
        Map<String, dynamic> json) =>
    _$_FetchWeatherParametersDto(
      json['latitude'] as String,
      json['longitude'] as String,
      json['current_weather'] as String? ?? 'true',
    );

Map<String, dynamic> _$$_FetchWeatherParametersDtoToJson(
        _$_FetchWeatherParametersDto instance) =>
    <String, dynamic>{
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'current_weather': instance.displayCurrentWeather,
    };
