// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'weather_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

WeatherDto _$WeatherDtoFromJson(Map<String, dynamic> json) {
  return _WeatherDto.fromJson(json);
}

/// @nodoc
class _$WeatherDtoTearOff {
  const _$WeatherDtoTearOff();

  _WeatherDto call(
      double temperature,
      @JsonKey(name: 'weathercode') int weatherCode,
      @JsonKey(name: 'windspeed') double windSpeed,
      @JsonKey(name: 'winddirection') double windDirection) {
    return _WeatherDto(
      temperature,
      weatherCode,
      windSpeed,
      windDirection,
    );
  }

  WeatherDto fromJson(Map<String, Object?> json) {
    return WeatherDto.fromJson(json);
  }
}

/// @nodoc
const $WeatherDto = _$WeatherDtoTearOff();

/// @nodoc
mixin _$WeatherDto {
  double get temperature => throw _privateConstructorUsedError;
  @JsonKey(name: 'weathercode')
  int get weatherCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'windspeed')
  double get windSpeed => throw _privateConstructorUsedError;
  @JsonKey(name: 'winddirection')
  double get windDirection => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WeatherDtoCopyWith<WeatherDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WeatherDtoCopyWith<$Res> {
  factory $WeatherDtoCopyWith(
          WeatherDto value, $Res Function(WeatherDto) then) =
      _$WeatherDtoCopyWithImpl<$Res>;
  $Res call(
      {double temperature,
      @JsonKey(name: 'weathercode') int weatherCode,
      @JsonKey(name: 'windspeed') double windSpeed,
      @JsonKey(name: 'winddirection') double windDirection});
}

/// @nodoc
class _$WeatherDtoCopyWithImpl<$Res> implements $WeatherDtoCopyWith<$Res> {
  _$WeatherDtoCopyWithImpl(this._value, this._then);

  final WeatherDto _value;
  // ignore: unused_field
  final $Res Function(WeatherDto) _then;

  @override
  $Res call({
    Object? temperature = freezed,
    Object? weatherCode = freezed,
    Object? windSpeed = freezed,
    Object? windDirection = freezed,
  }) {
    return _then(_value.copyWith(
      temperature: temperature == freezed
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as double,
      weatherCode: weatherCode == freezed
          ? _value.weatherCode
          : weatherCode // ignore: cast_nullable_to_non_nullable
              as int,
      windSpeed: windSpeed == freezed
          ? _value.windSpeed
          : windSpeed // ignore: cast_nullable_to_non_nullable
              as double,
      windDirection: windDirection == freezed
          ? _value.windDirection
          : windDirection // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$WeatherDtoCopyWith<$Res> implements $WeatherDtoCopyWith<$Res> {
  factory _$WeatherDtoCopyWith(
          _WeatherDto value, $Res Function(_WeatherDto) then) =
      __$WeatherDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {double temperature,
      @JsonKey(name: 'weathercode') int weatherCode,
      @JsonKey(name: 'windspeed') double windSpeed,
      @JsonKey(name: 'winddirection') double windDirection});
}

/// @nodoc
class __$WeatherDtoCopyWithImpl<$Res> extends _$WeatherDtoCopyWithImpl<$Res>
    implements _$WeatherDtoCopyWith<$Res> {
  __$WeatherDtoCopyWithImpl(
      _WeatherDto _value, $Res Function(_WeatherDto) _then)
      : super(_value, (v) => _then(v as _WeatherDto));

  @override
  _WeatherDto get _value => super._value as _WeatherDto;

  @override
  $Res call({
    Object? temperature = freezed,
    Object? weatherCode = freezed,
    Object? windSpeed = freezed,
    Object? windDirection = freezed,
  }) {
    return _then(_WeatherDto(
      temperature == freezed
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as double,
      weatherCode == freezed
          ? _value.weatherCode
          : weatherCode // ignore: cast_nullable_to_non_nullable
              as int,
      windSpeed == freezed
          ? _value.windSpeed
          : windSpeed // ignore: cast_nullable_to_non_nullable
              as double,
      windDirection == freezed
          ? _value.windDirection
          : windDirection // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
class _$_WeatherDto implements _WeatherDto {
  const _$_WeatherDto(
      this.temperature,
      @JsonKey(name: 'weathercode') this.weatherCode,
      @JsonKey(name: 'windspeed') this.windSpeed,
      @JsonKey(name: 'winddirection') this.windDirection);

  factory _$_WeatherDto.fromJson(Map<String, dynamic> json) =>
      _$$_WeatherDtoFromJson(json);

  @override
  final double temperature;
  @override
  @JsonKey(name: 'weathercode')
  final int weatherCode;
  @override
  @JsonKey(name: 'windspeed')
  final double windSpeed;
  @override
  @JsonKey(name: 'winddirection')
  final double windDirection;

  @override
  String toString() {
    return 'WeatherDto(temperature: $temperature, weatherCode: $weatherCode, windSpeed: $windSpeed, windDirection: $windDirection)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _WeatherDto &&
            const DeepCollectionEquality()
                .equals(other.temperature, temperature) &&
            const DeepCollectionEquality()
                .equals(other.weatherCode, weatherCode) &&
            const DeepCollectionEquality().equals(other.windSpeed, windSpeed) &&
            const DeepCollectionEquality()
                .equals(other.windDirection, windDirection));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(temperature),
      const DeepCollectionEquality().hash(weatherCode),
      const DeepCollectionEquality().hash(windSpeed),
      const DeepCollectionEquality().hash(windDirection));

  @JsonKey(ignore: true)
  @override
  _$WeatherDtoCopyWith<_WeatherDto> get copyWith =>
      __$WeatherDtoCopyWithImpl<_WeatherDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WeatherDtoToJson(this);
  }
}

abstract class _WeatherDto implements WeatherDto {
  const factory _WeatherDto(
      double temperature,
      @JsonKey(name: 'weathercode') int weatherCode,
      @JsonKey(name: 'windspeed') double windSpeed,
      @JsonKey(name: 'winddirection') double windDirection) = _$_WeatherDto;

  factory _WeatherDto.fromJson(Map<String, dynamic> json) =
      _$_WeatherDto.fromJson;

  @override
  double get temperature;
  @override
  @JsonKey(name: 'weathercode')
  int get weatherCode;
  @override
  @JsonKey(name: 'windspeed')
  double get windSpeed;
  @override
  @JsonKey(name: 'winddirection')
  double get windDirection;
  @override
  @JsonKey(ignore: true)
  _$WeatherDtoCopyWith<_WeatherDto> get copyWith =>
      throw _privateConstructorUsedError;
}
