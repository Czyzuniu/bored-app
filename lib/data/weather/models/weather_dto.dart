import 'package:freezed_annotation/freezed_annotation.dart';

part 'weather_dto.freezed.dart';
part 'weather_dto.g.dart';

@freezed
class WeatherDto with _$WeatherDto {
  @JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
  const factory WeatherDto(
      double temperature,
      @JsonKey(name: 'weathercode') final int weatherCode,
      @JsonKey(name: 'windspeed') final double windSpeed,
      @JsonKey(name: 'winddirection') final double windDirection) = _WeatherDto;


  factory WeatherDto.fromJson(Map<String, dynamic> json) =>
      _$WeatherDtoFromJson(json);
}
