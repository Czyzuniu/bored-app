import 'package:freezed_annotation/freezed_annotation.dart';

part 'fetch_weather_parameters_dto.freezed.dart';

part 'fetch_weather_parameters_dto.g.dart';

@freezed
class FetchWeatherParametersDto with _$FetchWeatherParametersDto {
  @JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
  const factory FetchWeatherParametersDto(
      String latitude,
      String longitude,
      [
        @Default('true')
        @JsonKey(
        name: 'current_weather',
        defaultValue: 'true',
      )
          String displayCurrentWeather]) = _FetchWeatherParametersDto;

  factory FetchWeatherParametersDto.fromJson(Map<String, dynamic> json) =>
      _$FetchWeatherParametersDtoFromJson(json);
}
