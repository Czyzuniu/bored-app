// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'get_weather_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

GetWeatherDto _$GetWeatherDtoFromJson(Map<String, dynamic> json) {
  return _GetWeatherDto.fromJson(json);
}

/// @nodoc
class _$GetWeatherDtoTearOff {
  const _$GetWeatherDtoTearOff();

  _GetWeatherDto call(@JsonKey(name: 'current_weather') WeatherDto weatherDto,
      double latitude, double longitude) {
    return _GetWeatherDto(
      weatherDto,
      latitude,
      longitude,
    );
  }

  GetWeatherDto fromJson(Map<String, Object?> json) {
    return GetWeatherDto.fromJson(json);
  }
}

/// @nodoc
const $GetWeatherDto = _$GetWeatherDtoTearOff();

/// @nodoc
mixin _$GetWeatherDto {
  @JsonKey(name: 'current_weather')
  WeatherDto get weatherDto => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GetWeatherDtoCopyWith<GetWeatherDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetWeatherDtoCopyWith<$Res> {
  factory $GetWeatherDtoCopyWith(
          GetWeatherDto value, $Res Function(GetWeatherDto) then) =
      _$GetWeatherDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'current_weather') WeatherDto weatherDto,
      double latitude,
      double longitude});

  $WeatherDtoCopyWith<$Res> get weatherDto;
}

/// @nodoc
class _$GetWeatherDtoCopyWithImpl<$Res>
    implements $GetWeatherDtoCopyWith<$Res> {
  _$GetWeatherDtoCopyWithImpl(this._value, this._then);

  final GetWeatherDto _value;
  // ignore: unused_field
  final $Res Function(GetWeatherDto) _then;

  @override
  $Res call({
    Object? weatherDto = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
  }) {
    return _then(_value.copyWith(
      weatherDto: weatherDto == freezed
          ? _value.weatherDto
          : weatherDto // ignore: cast_nullable_to_non_nullable
              as WeatherDto,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }

  @override
  $WeatherDtoCopyWith<$Res> get weatherDto {
    return $WeatherDtoCopyWith<$Res>(_value.weatherDto, (value) {
      return _then(_value.copyWith(weatherDto: value));
    });
  }
}

/// @nodoc
abstract class _$GetWeatherDtoCopyWith<$Res>
    implements $GetWeatherDtoCopyWith<$Res> {
  factory _$GetWeatherDtoCopyWith(
          _GetWeatherDto value, $Res Function(_GetWeatherDto) then) =
      __$GetWeatherDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'current_weather') WeatherDto weatherDto,
      double latitude,
      double longitude});

  @override
  $WeatherDtoCopyWith<$Res> get weatherDto;
}

/// @nodoc
class __$GetWeatherDtoCopyWithImpl<$Res>
    extends _$GetWeatherDtoCopyWithImpl<$Res>
    implements _$GetWeatherDtoCopyWith<$Res> {
  __$GetWeatherDtoCopyWithImpl(
      _GetWeatherDto _value, $Res Function(_GetWeatherDto) _then)
      : super(_value, (v) => _then(v as _GetWeatherDto));

  @override
  _GetWeatherDto get _value => super._value as _GetWeatherDto;

  @override
  $Res call({
    Object? weatherDto = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
  }) {
    return _then(_GetWeatherDto(
      weatherDto == freezed
          ? _value.weatherDto
          : weatherDto // ignore: cast_nullable_to_non_nullable
              as WeatherDto,
      latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
class _$_GetWeatherDto implements _GetWeatherDto {
  _$_GetWeatherDto(@JsonKey(name: 'current_weather') this.weatherDto,
      this.latitude, this.longitude);

  factory _$_GetWeatherDto.fromJson(Map<String, dynamic> json) =>
      _$$_GetWeatherDtoFromJson(json);

  @override
  @JsonKey(name: 'current_weather')
  final WeatherDto weatherDto;
  @override
  final double latitude;
  @override
  final double longitude;

  @override
  String toString() {
    return 'GetWeatherDto(weatherDto: $weatherDto, latitude: $latitude, longitude: $longitude)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _GetWeatherDto &&
            const DeepCollectionEquality()
                .equals(other.weatherDto, weatherDto) &&
            const DeepCollectionEquality().equals(other.latitude, latitude) &&
            const DeepCollectionEquality().equals(other.longitude, longitude));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(weatherDto),
      const DeepCollectionEquality().hash(latitude),
      const DeepCollectionEquality().hash(longitude));

  @JsonKey(ignore: true)
  @override
  _$GetWeatherDtoCopyWith<_GetWeatherDto> get copyWith =>
      __$GetWeatherDtoCopyWithImpl<_GetWeatherDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GetWeatherDtoToJson(this);
  }
}

abstract class _GetWeatherDto implements GetWeatherDto {
  factory _GetWeatherDto(
      @JsonKey(name: 'current_weather') WeatherDto weatherDto,
      double latitude,
      double longitude) = _$_GetWeatherDto;

  factory _GetWeatherDto.fromJson(Map<String, dynamic> json) =
      _$_GetWeatherDto.fromJson;

  @override
  @JsonKey(name: 'current_weather')
  WeatherDto get weatherDto;
  @override
  double get latitude;
  @override
  double get longitude;
  @override
  @JsonKey(ignore: true)
  _$GetWeatherDtoCopyWith<_GetWeatherDto> get copyWith =>
      throw _privateConstructorUsedError;
}
