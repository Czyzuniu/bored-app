// import 'package:bored/domain/core/errors/exceptions.dart';
// import 'package:location/location.dart';
//
// abstract class GetCurrentLocationDataSource {
//   Future<LocationData> getCurrentLocation();
// }
//
// class GetCurrentLocationDataSourceImpl implements GetCurrentLocationDataSource {
//   final Location location;
//
//   GetCurrentLocationDataSourceImpl(this.location);
//
//   @override
//   Future<LocationData> getCurrentLocation() async {
//
//     bool _serviceEnabled;
//     PermissionStatus _permissionGranted;
//
//     _serviceEnabled = await location.serviceEnabled();
//     if (!_serviceEnabled) {
//       _serviceEnabled = await location.requestService();
//       if (!_serviceEnabled) {
//         throw LocalizationDisabledException();
//       }
//     }
//
//     _permissionGranted = await location.hasPermission();
//     if (_permissionGranted == PermissionStatus.denied) {
//       _permissionGranted = await location.requestPermission();
//       if (_permissionGranted != PermissionStatus.granted) {
//         throw LocalizationPermissionDeniedException();
//       }
//     }
//
//     return location.getLocation();
//   }
// }