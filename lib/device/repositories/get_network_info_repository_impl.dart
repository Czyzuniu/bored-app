import 'package:bored/domain/core/repositories/get_network_info_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

@LazySingleton(as: GetNetworkInfoRepository)
class NetworkInfoImpl implements GetNetworkInfoRepository {
  final InternetConnectionChecker connectionChecker;

  NetworkInfoImpl(this.connectionChecker);

  @override
  Future<bool> get isConnected => connectionChecker.hasConnection;
}
