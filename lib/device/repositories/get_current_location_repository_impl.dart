import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/core/repositories/get_current_location_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:location/location.dart';

@LazySingleton(as: GetCurrentLocalizationRepository)
class GetCurrentLocationRepositoryImpl
    implements GetCurrentLocalizationRepository {
  final Location location;

  GetCurrentLocationRepositoryImpl(this.location);

  @override
  Future<Either<Failure, CoordinativeLocation>> getLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return const Left<Failure, CoordinativeLocation>(
            Failure.localizationDisabled(
                'Your localization services are disabled, please enable them.'));
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return const Left<Failure, CoordinativeLocation>(
            Failure.permissionDenied('Please allow your location'));
      }
    }

    final result = await location.getLocation();

    return Right(CoordinativeLocation(
        longitude: result.longitude ?? 0.0, latitude: result.latitude ?? 0.0));
  }
}
