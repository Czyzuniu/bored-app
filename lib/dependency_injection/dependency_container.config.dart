// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:http/http.dart' as _i4;
import 'package:injectable/injectable.dart' as _i2;
import 'package:internet_connection_checker/internet_connection_checker.dart'
    as _i11;
import 'package:location/location.dart' as _i12;

import '../data/find_activity/datasources/find_activity_remote_data_source.dart'
    as _i6;
import '../data/find_activity/mapper/activity_dto_mapper.dart' as _i3;
import '../data/find_activity/mapper/search_activity_dto_mapper.dart' as _i13;
import '../data/find_activity/repositories/find_activity_repository_impl.dart'
    as _i21;
import '../data/weather/datasources/get_weather_remote_data_source.dart' as _i7;
import '../data/weather/repositories/get_weather_repository_impl.dart' as _i9;
import '../device/repositories/get_current_location_repository_impl.dart'
    as _i15;
import '../device/repositories/get_network_info_repository_impl.dart' as _i17;
import '../domain/core/repositories/get_current_location_repository.dart'
    as _i14;
import '../domain/core/repositories/get_network_info_repository.dart' as _i16;
import '../domain/find_activity/repositories/find_activity_repository.dart'
    as _i20;
import '../domain/find_activity/usecases/find_activity.dart' as _i23;
import '../domain/find_activity/usecases/find_random_activity.dart' as _i22;
import '../domain/utils/input_converter.dart' as _i10;
import '../domain/weather/repositories/get_weather_repository.dart' as _i8;
import '../domain/weather/usecase/get_weather_by_localization.dart' as _i18;
import '../presentation/find_activity/bloc/find_activity_input/find_activity_input_bloc.dart'
    as _i5;
import '../presentation/find_activity/bloc/find_activity_submit/find_activity_submit_bloc.dart'
    as _i24;
import '../presentation/weather/cubit/weather_cubit.dart' as _i19;
import 'dependency_container_injection_module.dart'
    as _i25; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final dependencyContainerInjectionModule =
      _$DependencyContainerInjectionModule();
  gh.lazySingleton<_i3.ActivityDtoMapper>(() => _i3.ActivityDtoMapperImpl());
  gh.factory<_i4.Client>(() => dependencyContainerInjectionModule.httpClient);
  gh.factory<_i5.FindActivityInputBloc>(() => _i5.FindActivityInputBloc());
  gh.lazySingleton<_i6.FindActivityRemoteDataSource>(
      () => _i6.FindActivityRemoteDataSourceImpl(client: get<_i4.Client>()));
  gh.lazySingleton<_i7.GetWeatherRemoteDataSource>(
      () => _i7.GetWeatherRemoteDataSourceImpl(get<_i4.Client>()));
  gh.lazySingleton<_i8.GetWeatherRepository>(() =>
      _i9.GetWeatherRepositoryImpl(get<_i7.GetWeatherRemoteDataSource>()));
  gh.lazySingleton<_i10.InputConverter>(() => _i10.InputConverter());
  gh.factory<_i11.InternetConnectionChecker>(
      () => dependencyContainerInjectionModule.connectionChecker);
  gh.factory<_i12.Location>(() => dependencyContainerInjectionModule.location);
  gh.lazySingleton<_i13.SearchActivityMapper>(
      () => _i13.SearchActivityMapperImpl());
  gh.lazySingleton<_i14.GetCurrentLocalizationRepository>(
      () => _i15.GetCurrentLocationRepositoryImpl(get<_i12.Location>()));
  gh.lazySingleton<_i16.GetNetworkInfoRepository>(
      () => _i17.NetworkInfoImpl(get<_i11.InternetConnectionChecker>()));
  gh.lazySingleton<_i18.GetWeatherByLocalization>(() =>
      _i18.GetWeatherByLocalization(get<_i8.GetWeatherRepository>(),
          get<_i14.GetCurrentLocalizationRepository>()));
  gh.lazySingleton<_i19.WeatherCubit>(
      () => _i19.WeatherCubit(get<_i18.GetWeatherByLocalization>()));
  gh.lazySingleton<_i20.FindActivityRepository>(() =>
      _i21.FindActivityRepositoryImpl(
          remoteDataSource: get<_i6.FindActivityRemoteDataSource>(),
          networkInfo: get<_i16.GetNetworkInfoRepository>(),
          activityDtoMapper: get<_i3.ActivityDtoMapper>(),
          searchActivityMapper: get<_i13.SearchActivityMapper>()));
  gh.lazySingleton<_i22.FindRandomActivity>(
      () => _i22.FindRandomActivity(get<_i20.FindActivityRepository>()));
  gh.lazySingleton<_i23.FindActivity>(
      () => _i23.FindActivity(get<_i20.FindActivityRepository>()));
  gh.factory<_i24.FindActivitySubmitBloc>(() => _i24.FindActivitySubmitBloc(
      get<_i23.FindActivity>(),
      get<_i10.InputConverter>(),
      get<_i22.FindRandomActivity>()));
  return get;
}

class _$DependencyContainerInjectionModule
    extends _i25.DependencyContainerInjectionModule {}
