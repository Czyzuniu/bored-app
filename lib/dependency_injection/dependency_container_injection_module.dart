import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:location/location.dart';

@module
abstract class DependencyContainerInjectionModule {
  http.Client get httpClient => http.Client();
  InternetConnectionChecker get connectionChecker => InternetConnectionChecker();
  Location get location => Location();
}