import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/core/repositories/get_current_location_repository.dart';
import 'package:bored/domain/weather/entities/weather.dart';
import 'package:bored/domain/weather/repositories/get_weather_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@LazySingleton()
class GetWeatherByLocalization {
  final GetCurrentLocalizationRepository localizationRepository;
  final GetWeatherRepository repository;

  GetWeatherByLocalization(this.repository, this.localizationRepository);

  Future<Either<Failure, Weather>> execute() async {
    final currentLocation = await localizationRepository.getLocation();
    return currentLocation.fold(
        (l) => Left(l), (r) => repository.getWeatherByCoordinate(r));
  }
}
