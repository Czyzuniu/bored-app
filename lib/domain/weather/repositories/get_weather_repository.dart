import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/weather/entities/weather.dart';
import 'package:dartz/dartz.dart';

abstract class GetWeatherRepository {
  Future<Either<Failure, Weather>> getWeatherByCoordinate(CoordinativeLocation coords);
}