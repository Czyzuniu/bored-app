abstract class GetNetworkInfoRepository {
  Future<bool> get isConnected;
}