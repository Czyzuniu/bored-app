import 'package:bored/domain/core/entities/coordinative_location.dart';
import 'package:bored/domain/core/errors/failure.dart';
import 'package:dartz/dartz.dart';

abstract class GetCurrentLocalizationRepository {
  Future<Either<Failure, CoordinativeLocation>> getLocation();
}