import 'package:freezed_annotation/freezed_annotation.dart';

part 'coordinative_location.freezed.dart';

@freezed
class CoordinativeLocation with _$CoordinativeLocation {
  const CoordinativeLocation._();

  const factory CoordinativeLocation(
      {required double latitude,
      required double longitude}) = _CoordinativeLocation;


  String getLatitudeAsString() {
    return latitude.toStringAsFixed(2);
  }

  String getLongitudeAsString() {
    return longitude.toStringAsFixed(2);
  }
}
