import 'package:freezed_annotation/freezed_annotation.dart';

part 'failure.freezed.dart';

@freezed
class Failure with _$Failure {
  const factory Failure.server(String message) = _ServerFailure;
  const factory Failure.noInternet(String message) = _NoInternet;
  const factory Failure.invalidInput(String message) = _InvalidInput;
  const factory Failure.noActivityFound(String message) = _NoActivityFound;
  const factory Failure.localizationDisabled(String message) = _LocalizationDisabled;
  const factory Failure.permissionDenied(String message) = _PermissionDenied;

}
