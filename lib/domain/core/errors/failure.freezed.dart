// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FailureTearOff {
  const _$FailureTearOff();

  _ServerFailure server(String message) {
    return _ServerFailure(
      message,
    );
  }

  _NoInternet noInternet(String message) {
    return _NoInternet(
      message,
    );
  }

  _InvalidInput invalidInput(String message) {
    return _InvalidInput(
      message,
    );
  }

  _NoActivityFound noActivityFound(String message) {
    return _NoActivityFound(
      message,
    );
  }

  _LocalizationDisabled localizationDisabled(String message) {
    return _LocalizationDisabled(
      message,
    );
  }

  _PermissionDenied permissionDenied(String message) {
    return _PermissionDenied(
      message,
    );
  }
}

/// @nodoc
const $Failure = _$FailureTearOff();

/// @nodoc
mixin _$Failure {
  String get message => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FailureCopyWith<Failure> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureCopyWith<$Res> {
  factory $FailureCopyWith(Failure value, $Res Function(Failure) then) =
      _$FailureCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class _$FailureCopyWithImpl<$Res> implements $FailureCopyWith<$Res> {
  _$FailureCopyWithImpl(this._value, this._then);

  final Failure _value;
  // ignore: unused_field
  final $Res Function(Failure) _then;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$ServerFailureCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory _$ServerFailureCopyWith(
          _ServerFailure value, $Res Function(_ServerFailure) then) =
      __$ServerFailureCopyWithImpl<$Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class __$ServerFailureCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements _$ServerFailureCopyWith<$Res> {
  __$ServerFailureCopyWithImpl(
      _ServerFailure _value, $Res Function(_ServerFailure) _then)
      : super(_value, (v) => _then(v as _ServerFailure));

  @override
  _ServerFailure get _value => super._value as _ServerFailure;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_ServerFailure(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ServerFailure implements _ServerFailure {
  const _$_ServerFailure(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'Failure.server(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ServerFailure &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$ServerFailureCopyWith<_ServerFailure> get copyWith =>
      __$ServerFailureCopyWithImpl<_ServerFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) {
    return server(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) {
    return server?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) {
    if (server != null) {
      return server(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return server(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) {
    return server?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (server != null) {
      return server(this);
    }
    return orElse();
  }
}

abstract class _ServerFailure implements Failure {
  const factory _ServerFailure(String message) = _$_ServerFailure;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$ServerFailureCopyWith<_ServerFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NoInternetCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory _$NoInternetCopyWith(
          _NoInternet value, $Res Function(_NoInternet) then) =
      __$NoInternetCopyWithImpl<$Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class __$NoInternetCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements _$NoInternetCopyWith<$Res> {
  __$NoInternetCopyWithImpl(
      _NoInternet _value, $Res Function(_NoInternet) _then)
      : super(_value, (v) => _then(v as _NoInternet));

  @override
  _NoInternet get _value => super._value as _NoInternet;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_NoInternet(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_NoInternet implements _NoInternet {
  const _$_NoInternet(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'Failure.noInternet(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _NoInternet &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$NoInternetCopyWith<_NoInternet> get copyWith =>
      __$NoInternetCopyWithImpl<_NoInternet>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) {
    return noInternet(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) {
    return noInternet?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) {
    if (noInternet != null) {
      return noInternet(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return noInternet(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) {
    return noInternet?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (noInternet != null) {
      return noInternet(this);
    }
    return orElse();
  }
}

abstract class _NoInternet implements Failure {
  const factory _NoInternet(String message) = _$_NoInternet;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$NoInternetCopyWith<_NoInternet> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$InvalidInputCopyWith<$Res> implements $FailureCopyWith<$Res> {
  factory _$InvalidInputCopyWith(
          _InvalidInput value, $Res Function(_InvalidInput) then) =
      __$InvalidInputCopyWithImpl<$Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class __$InvalidInputCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements _$InvalidInputCopyWith<$Res> {
  __$InvalidInputCopyWithImpl(
      _InvalidInput _value, $Res Function(_InvalidInput) _then)
      : super(_value, (v) => _then(v as _InvalidInput));

  @override
  _InvalidInput get _value => super._value as _InvalidInput;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_InvalidInput(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_InvalidInput implements _InvalidInput {
  const _$_InvalidInput(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'Failure.invalidInput(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _InvalidInput &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$InvalidInputCopyWith<_InvalidInput> get copyWith =>
      __$InvalidInputCopyWithImpl<_InvalidInput>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) {
    return invalidInput(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) {
    return invalidInput?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) {
    if (invalidInput != null) {
      return invalidInput(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return invalidInput(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) {
    return invalidInput?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (invalidInput != null) {
      return invalidInput(this);
    }
    return orElse();
  }
}

abstract class _InvalidInput implements Failure {
  const factory _InvalidInput(String message) = _$_InvalidInput;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$InvalidInputCopyWith<_InvalidInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NoActivityFoundCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$NoActivityFoundCopyWith(
          _NoActivityFound value, $Res Function(_NoActivityFound) then) =
      __$NoActivityFoundCopyWithImpl<$Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class __$NoActivityFoundCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements _$NoActivityFoundCopyWith<$Res> {
  __$NoActivityFoundCopyWithImpl(
      _NoActivityFound _value, $Res Function(_NoActivityFound) _then)
      : super(_value, (v) => _then(v as _NoActivityFound));

  @override
  _NoActivityFound get _value => super._value as _NoActivityFound;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_NoActivityFound(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_NoActivityFound implements _NoActivityFound {
  const _$_NoActivityFound(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'Failure.noActivityFound(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _NoActivityFound &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$NoActivityFoundCopyWith<_NoActivityFound> get copyWith =>
      __$NoActivityFoundCopyWithImpl<_NoActivityFound>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) {
    return noActivityFound(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) {
    return noActivityFound?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) {
    if (noActivityFound != null) {
      return noActivityFound(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return noActivityFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) {
    return noActivityFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (noActivityFound != null) {
      return noActivityFound(this);
    }
    return orElse();
  }
}

abstract class _NoActivityFound implements Failure {
  const factory _NoActivityFound(String message) = _$_NoActivityFound;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$NoActivityFoundCopyWith<_NoActivityFound> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$LocalizationDisabledCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$LocalizationDisabledCopyWith(_LocalizationDisabled value,
          $Res Function(_LocalizationDisabled) then) =
      __$LocalizationDisabledCopyWithImpl<$Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class __$LocalizationDisabledCopyWithImpl<$Res>
    extends _$FailureCopyWithImpl<$Res>
    implements _$LocalizationDisabledCopyWith<$Res> {
  __$LocalizationDisabledCopyWithImpl(
      _LocalizationDisabled _value, $Res Function(_LocalizationDisabled) _then)
      : super(_value, (v) => _then(v as _LocalizationDisabled));

  @override
  _LocalizationDisabled get _value => super._value as _LocalizationDisabled;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_LocalizationDisabled(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_LocalizationDisabled implements _LocalizationDisabled {
  const _$_LocalizationDisabled(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'Failure.localizationDisabled(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _LocalizationDisabled &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$LocalizationDisabledCopyWith<_LocalizationDisabled> get copyWith =>
      __$LocalizationDisabledCopyWithImpl<_LocalizationDisabled>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) {
    return localizationDisabled(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) {
    return localizationDisabled?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) {
    if (localizationDisabled != null) {
      return localizationDisabled(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return localizationDisabled(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) {
    return localizationDisabled?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (localizationDisabled != null) {
      return localizationDisabled(this);
    }
    return orElse();
  }
}

abstract class _LocalizationDisabled implements Failure {
  const factory _LocalizationDisabled(String message) = _$_LocalizationDisabled;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$LocalizationDisabledCopyWith<_LocalizationDisabled> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$PermissionDeniedCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$PermissionDeniedCopyWith(
          _PermissionDenied value, $Res Function(_PermissionDenied) then) =
      __$PermissionDeniedCopyWithImpl<$Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class __$PermissionDeniedCopyWithImpl<$Res> extends _$FailureCopyWithImpl<$Res>
    implements _$PermissionDeniedCopyWith<$Res> {
  __$PermissionDeniedCopyWithImpl(
      _PermissionDenied _value, $Res Function(_PermissionDenied) _then)
      : super(_value, (v) => _then(v as _PermissionDenied));

  @override
  _PermissionDenied get _value => super._value as _PermissionDenied;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_PermissionDenied(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_PermissionDenied implements _PermissionDenied {
  const _$_PermissionDenied(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'Failure.permissionDenied(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PermissionDenied &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$PermissionDeniedCopyWith<_PermissionDenied> get copyWith =>
      __$PermissionDeniedCopyWithImpl<_PermissionDenied>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) server,
    required TResult Function(String message) noInternet,
    required TResult Function(String message) invalidInput,
    required TResult Function(String message) noActivityFound,
    required TResult Function(String message) localizationDisabled,
    required TResult Function(String message) permissionDenied,
  }) {
    return permissionDenied(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
  }) {
    return permissionDenied?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? server,
    TResult Function(String message)? noInternet,
    TResult Function(String message)? invalidInput,
    TResult Function(String message)? noActivityFound,
    TResult Function(String message)? localizationDisabled,
    TResult Function(String message)? permissionDenied,
    required TResult orElse(),
  }) {
    if (permissionDenied != null) {
      return permissionDenied(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerFailure value) server,
    required TResult Function(_NoInternet value) noInternet,
    required TResult Function(_InvalidInput value) invalidInput,
    required TResult Function(_NoActivityFound value) noActivityFound,
    required TResult Function(_LocalizationDisabled value) localizationDisabled,
    required TResult Function(_PermissionDenied value) permissionDenied,
  }) {
    return permissionDenied(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
  }) {
    return permissionDenied?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerFailure value)? server,
    TResult Function(_NoInternet value)? noInternet,
    TResult Function(_InvalidInput value)? invalidInput,
    TResult Function(_NoActivityFound value)? noActivityFound,
    TResult Function(_LocalizationDisabled value)? localizationDisabled,
    TResult Function(_PermissionDenied value)? permissionDenied,
    required TResult orElse(),
  }) {
    if (permissionDenied != null) {
      return permissionDenied(this);
    }
    return orElse();
  }
}

abstract class _PermissionDenied implements Failure {
  const factory _PermissionDenied(String message) = _$_PermissionDenied;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$PermissionDeniedCopyWith<_PermissionDenied> get copyWith =>
      throw _privateConstructorUsedError;
}
