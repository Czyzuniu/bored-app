class ServerException implements Exception {
  final String message;

  ServerException(this.message);
}

class NoInternetException implements Exception {}

class NoRecordException implements Exception {
  final String message;

  NoRecordException(this.message);
}

class CacheException implements Exception {}

class LocalizationDisabledException implements Exception {}

class LocalizationPermissionDeniedException implements Exception {}
