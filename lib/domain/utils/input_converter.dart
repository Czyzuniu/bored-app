import 'package:bored/domain/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@LazySingleton()
class InputConverter {
  Either<Failure, int> stringToPositiveInteger(String inputString) {
    try {
      final parsed = int.parse(inputString);
      if (parsed > 0) {
        return Right(parsed);
      }
      throw const FormatException();
    } on FormatException {
      return const Left(Failure.invalidInput('Unable to parse the number.'));
    }
  }
}
