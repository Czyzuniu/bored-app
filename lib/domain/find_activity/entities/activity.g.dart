// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Activity _$$_ActivityFromJson(Map<String, dynamic> json) => _$_Activity(
      activity: json['activity'] as String,
      activityType: $enumDecode(_$ActivityTypeEnumMap, json['activity_type']),
      participants: json['participants'] as int,
    );

Map<String, dynamic> _$$_ActivityToJson(_$_Activity instance) =>
    <String, dynamic>{
      'activity': instance.activity,
      'activity_type': _$ActivityTypeEnumMap[instance.activityType],
      'participants': instance.participants,
    };

const _$ActivityTypeEnumMap = {
  ActivityType.recreational: 'recreational',
  ActivityType.education: 'education',
  ActivityType.social: 'social',
  ActivityType.relaxation: 'relaxation',
  ActivityType.cooking: 'cooking',
  ActivityType.diy: 'diy',
  ActivityType.music: 'music',
  ActivityType.busywork: 'busywork',
  ActivityType.charity: 'charity',
};
