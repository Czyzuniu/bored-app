import 'package:freezed_annotation/freezed_annotation.dart';

import 'activity_type.dart';

part 'search_activity.freezed.dart';

@freezed
class SearchActivity with _$SearchActivity {
  const factory SearchActivity({
    required ActivityType activityType,
    required int participants
  }) = _SearchActivity;
}