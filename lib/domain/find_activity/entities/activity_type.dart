enum ActivityType {
  recreational, education, social, relaxation, cooking, diy, music, busywork, charity
}