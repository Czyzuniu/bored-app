// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'search_activity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SearchActivityTearOff {
  const _$SearchActivityTearOff();

  _SearchActivity call(
      {required ActivityType activityType, required int participants}) {
    return _SearchActivity(
      activityType: activityType,
      participants: participants,
    );
  }
}

/// @nodoc
const $SearchActivity = _$SearchActivityTearOff();

/// @nodoc
mixin _$SearchActivity {
  ActivityType get activityType => throw _privateConstructorUsedError;
  int get participants => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SearchActivityCopyWith<SearchActivity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchActivityCopyWith<$Res> {
  factory $SearchActivityCopyWith(
          SearchActivity value, $Res Function(SearchActivity) then) =
      _$SearchActivityCopyWithImpl<$Res>;
  $Res call({ActivityType activityType, int participants});
}

/// @nodoc
class _$SearchActivityCopyWithImpl<$Res>
    implements $SearchActivityCopyWith<$Res> {
  _$SearchActivityCopyWithImpl(this._value, this._then);

  final SearchActivity _value;
  // ignore: unused_field
  final $Res Function(SearchActivity) _then;

  @override
  $Res call({
    Object? activityType = freezed,
    Object? participants = freezed,
  }) {
    return _then(_value.copyWith(
      activityType: activityType == freezed
          ? _value.activityType
          : activityType // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$SearchActivityCopyWith<$Res>
    implements $SearchActivityCopyWith<$Res> {
  factory _$SearchActivityCopyWith(
          _SearchActivity value, $Res Function(_SearchActivity) then) =
      __$SearchActivityCopyWithImpl<$Res>;
  @override
  $Res call({ActivityType activityType, int participants});
}

/// @nodoc
class __$SearchActivityCopyWithImpl<$Res>
    extends _$SearchActivityCopyWithImpl<$Res>
    implements _$SearchActivityCopyWith<$Res> {
  __$SearchActivityCopyWithImpl(
      _SearchActivity _value, $Res Function(_SearchActivity) _then)
      : super(_value, (v) => _then(v as _SearchActivity));

  @override
  _SearchActivity get _value => super._value as _SearchActivity;

  @override
  $Res call({
    Object? activityType = freezed,
    Object? participants = freezed,
  }) {
    return _then(_SearchActivity(
      activityType: activityType == freezed
          ? _value.activityType
          : activityType // ignore: cast_nullable_to_non_nullable
              as ActivityType,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SearchActivity implements _SearchActivity {
  const _$_SearchActivity(
      {required this.activityType, required this.participants});

  @override
  final ActivityType activityType;
  @override
  final int participants;

  @override
  String toString() {
    return 'SearchActivity(activityType: $activityType, participants: $participants)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SearchActivity &&
            const DeepCollectionEquality()
                .equals(other.activityType, activityType) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(activityType),
      const DeepCollectionEquality().hash(participants));

  @JsonKey(ignore: true)
  @override
  _$SearchActivityCopyWith<_SearchActivity> get copyWith =>
      __$SearchActivityCopyWithImpl<_SearchActivity>(this, _$identity);
}

abstract class _SearchActivity implements SearchActivity {
  const factory _SearchActivity(
      {required ActivityType activityType,
      required int participants}) = _$_SearchActivity;

  @override
  ActivityType get activityType;
  @override
  int get participants;
  @override
  @JsonKey(ignore: true)
  _$SearchActivityCopyWith<_SearchActivity> get copyWith =>
      throw _privateConstructorUsedError;
}
