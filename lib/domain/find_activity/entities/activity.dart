import 'package:freezed_annotation/freezed_annotation.dart';

import 'activity_type.dart';

part 'activity.freezed.dart';
part 'activity.g.dart';

@freezed
class Activity with _$Activity {
  @JsonSerializable(fieldRename: FieldRename.snake, explicitToJson: true)
  const factory Activity({
     required String activity, required ActivityType activityType, required int participants
  }) = _Activity;
  

  factory Activity.fromJson(Map<String, dynamic> json) =>
      _$ActivityFromJson(json);
}