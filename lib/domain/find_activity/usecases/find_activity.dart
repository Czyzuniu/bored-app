import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:bored/domain/find_activity/repositories/find_activity_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';


@LazySingleton()
class FindActivity {
  final FindActivityRepository repository;

  FindActivity(this.repository);

  Future<Either<Failure, Activity>>execute(SearchActivity searchActivity) async {
    return await repository.findActivity(searchActivity);
  }
}