import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/repositories/find_activity_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';


@LazySingleton()
class FindRandomActivity {
  final FindActivityRepository repository;

  FindRandomActivity(this.repository);

  Future<Either<Failure, Activity>>execute() async {
    return await repository.findRandomActivity();
  }
}