import 'package:bored/domain/core/errors/failure.dart';
import 'package:bored/domain/find_activity/entities/activity.dart';
import 'package:bored/domain/find_activity/entities/search_activity.dart';
import 'package:dartz/dartz.dart';


abstract class FindActivityRepository {
  Future<Either<Failure, Activity>> findActivity(SearchActivity findActivityDto);
  Future<Either<Failure, Activity>> findRandomActivity();
}